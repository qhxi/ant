# ant

### 账户类型

1. 公司运营账户，需要自己的比特币地址，1)用以与普通用户转帐交易，2)在交易所平台交易,提现
2. 普通使用者帐号，与公司运营帐号转帐、存入现金、提现金等。

### 交易流程
##### 流程
1. 买入流程

![买入流程](./buy.png)

2. 卖出流程

![卖出流程](./sell.png)

3. 提现流程

![卖出流程](./reminder.png)


### API

1. 查询汇率(暂定)  
  [https://blockchain.info/ticker](https://blockchain.info/ticker)

返回：
  ```
      {
      "USD" : {"15m" : 5901.2, "last" : 5901.2, "buy" : 5901.2, "sell" : 5901.2, "symbol" : "$"},
      "AUD" : {"15m" : 7986.04, "last" : 7986.04, "buy" : 7986.04, "sell" : 7986.04, "symbol" : "$"},
      "BRL" : {"15m" : 22795.14, "last" : 22795.14, "buy" : 22795.14, "sell" : 22795.14, "symbol" : "R$"},
      "CAD" : {"15m" : 7805.84, "last" : 7805.84, "buy" : 7805.84, "sell" : 7805.84, "symbol" : "$"},
      "CHF" : {"15m" : 5869.41, "last" : 5869.41, "buy" : 5869.41, "sell" : 5869.41, "symbol" : "CHF"},
      "CLP" : {"15m" : 3822204.92, "last" : 3822204.92, "buy" : 3822204.92, "sell" : 3822204.92, "symbol" : "$"},
      "CNY" : {"15m" : 39058.25, "last" : 39058.25, "buy" : 39058.25, "sell" : 39058.25, "symbol" : "¥"},
      "DKK" : {"15m" : 37802.42, "last" : 37802.42, "buy" : 37802.42, "sell" : 37802.42, "symbol" : "kr"},
      "EUR" : {"15m" : 5066.71, "last" : 5066.71, "buy" : 5066.71, "sell" : 5066.71, "symbol" : "€"},
      "GBP" : {"15m" : 4502.31, "last" : 4502.31, "buy" : 4502.31, "sell" : 4502.31, "symbol" : "£"},
      "HKD" : {"15m" : 46310.88, "last" : 46310.88, "buy" : 46310.88, "sell" : 46310.88, "symbol" : "$"},
      "INR" : {"15m" : 404763.06, "last" : 404763.06, "buy" : 404763.06, "sell" : 404763.06, "symbol" : "₹"},
      "ISK" : {"15m" : 630011.73, "last" : 630011.73, "buy" : 630011.73, "sell" : 630011.73, "symbol" : "kr"},
      "JPY" : {"15m" : 652011.34, "last" : 652011.34, "buy" : 652011.34, "sell" : 652011.34, "symbol" : "¥"},
      "KRW" : {"15m" : 6581545.36, "last" : 6581545.36, "buy" : 6581545.36, "sell" : 6581545.36, "symbol" : "₩"},
      "NZD" : {"15m" : 8719.55, "last" : 8719.55, "buy" : 8719.55, "sell" : 8719.55, "symbol" : "$"},
      "PLN" : {"15m" : 22097.89, "last" : 22097.89, "buy" : 22097.89, "sell" : 22097.89, "symbol" : "zł"},
      "RUB" : {"15m" : 369994.69, "last" : 369994.69, "buy" : 369994.69, "sell" : 369994.69, "symbol" : "RUB"},
      "SEK" : {"15m" : 52971.31, "last" : 52971.31, "buy" : 52971.31, "sell" : 52971.31, "symbol" : "kr"},
      "SGD" : {"15m" : 8048.85, "last" : 8048.85, "buy" : 8048.85, "sell" : 8048.85, "symbol" : "$"},
      "THB" : {"15m" : 195524.34, "last" : 195524.34, "buy" : 195524.34, "sell" : 195524.34, "symbol" : "฿"},
      "TWD" : {"15m" : 179809.45, "last" : 179809.45, "buy" : 179809.45, "sell" : 179809.45, "symbol" : "NT$"}
    }
```
2. 现金到货币转换  
  [https://blockchain.info/tobtc]

3. xxxx

### Contribution

1. Fork the project
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [http://git.mydoc.io/](http://git.mydoc.io/)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
