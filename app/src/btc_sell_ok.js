'use strict';
const remote = require('electron').remote
const QRCode = require('qrcode')
const ipc = require('electron').ipcRenderer
const BrowserWindow = require('electron').remote.BrowserWindow

let timer
let Note1 = remote.getGlobal('shareData').out_channel1
let Note2 = remote.getGlobal('shareData').out_channel2

$(function () {
    const btc_ok = $('#btc_ok')
    btc_ok.on('click', (e) => {
        remote.getGlobal('shareData').bills = 0;
        remote.getGlobal('shareData').gamount = 0;
        document.location.href = "../index.html";
    });
    btc_ok.hide()

    var txid = remote.getGlobal('shareData').gtxid
    $("#tx_id").html(txid);
    var canvas = document.getElementById('canvas');
    QRCode.toCanvas(canvas, txid, function (error) {
        if (error) console.error(error);
        console.log('success!');
    });

    timer = setInterval(query, 5000);
    //time out
    setTimeout(function(){
        document.location.replace("../index.html");
    }, 1200000);
})

function query(){
    var gcoin = remote.getGlobal('shareData').gcoin
    var gatmid = remote.getGlobal('shareData').gatmid
    var txid = remote.getGlobal('shareData').gtxid
    var host = remote.getGlobal('shareData').host
    $.getJSON(host + gcoin + "/querytx?atmid=" + gatmid + "&txpid=" + txid, function(data){
        if(Number(data.status) === 1 && data.data.txp.action === "received"){
            var confirms = Number(data.data.txp.confirmations);
            console.log('confirms is:' + confirms + 'txpid:' + data.data.txp.txid)
            if (confirms > 0){
                clearInterval(timer);
                console.log('get transaction:' + host + "users/gettransaction?atmid=" + gatmid + "&txid=" + txid);
                $.getJSON(host + "users/gettransaction?atmid=" + gatmid + "&txid=" + txid, function(ret){
                    if(Number(ret.status) === 1 && ret.data.txid != null && ret.data.bills != null){
                        //txp time is less then
                        $('#btc_ok').show()
                        //payout the money
                        console.log('transaction is done' + JSON.stringify(ret));
                        smartpayout_pay(ret.data.bills)
                        var sd = remote.getGlobal('shareData')
                        var from = data.data.txp.to; //data.data.txp.outputs[0].address
                        if (!from){
                            from = "unknown"
                        }
                        var mydata = {actionid:'100', atmid:ret.data.atmid, timestamp:Date.now(), amount_coin:ret.data.amount, amount:ret.data.bills, txpid:txid, fee:data.data.txp.fees, from:from, to:'atmaccount', action:'received', currency:ret.data.currency, coin:ret.data.coin, rate:sd.gbrate}
                        $.post(host + "users/log", JSON.stringify(mydata), function(data1){
                            console.log('log something with result:' + data1.status);
                        })        
                    }else{
                        $('#btc_ok').show()
                        $('#tips').html("this transaction is already done!contact us if have problem!")
                    }
                });
            }
        }
    })
}

function smartpayout_pay(amount){
    var amount2 = (amount - (amount % Note2)) / Note2;
    amount = amount % Note2;
    var amount1 = (amount - (amount % Note1)) / Note2;
    var winid = ipc.sendSync('get-devicewin-id', '');
    if(winid !== undefined && winid > 0){
        var dwin = BrowserWindow.fromId(Number(winid))
        if(dwin){
            dwin.webContents.send('sdm2000-pay', amount1, amount2, remote.getCurrentWindow().id);
        }
    }
}

ipc.on('sdm2000-pay-res', (event, err, arg) =>{
    console.log('sdm2000 get status return:' + arg);
})