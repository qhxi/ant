'use strict';
const remote = require('electron').remote
const dialog = remote.dialog
const QRCode = require('qrcode')
const ipc = require('electron').ipcRenderer

//global.coin_type global.amount global.rate
var txpid;

$(function () {
    const buy_ok = $('#buy_ok_ok')
    buy_ok.on('click', (e) => {
        remote.getGlobal('shareData').bills = 0;
        remote.getGlobal('shareData').gamount = 0;
        document.location.href = "../index.html";
    });
    buy_ok.hide()

    update_ui();

    var gamount = remote.getGlobal('shareData').gamount
    var gcoin = remote.getGlobal('shareData').gcoin
    var gatmid = remote.getGlobal('shareData').gatmid
    var address = remote.getGlobal('shareData').save_address
    var host = remote.getGlobal('shareData').host
    
    ipc.send('start_loading');
    $.getJSON(host + gcoin + "/paytoqueue?atmid=" + gatmid +"&address=" + address + "&amount=" + gamount).done(function(data){
        if(Number(data.status) === 1){
            console.log("transaction is ok:", data.data.txp)
            var sd = remote.getGlobal('shareData')
            txpid = data.data.txp.txpid;
            //print ticket
            sd.gtxid = txpid;
            ipc.send('print-trans', '');
            // var data = {actionid:'100', atmid:sd.gatmid, timestamp:Date.now, amount_coin:sd.gamount, amount:sd.bills, txpid:data.data.txp.txid, input:data.data.txp.inputs, output:data.data.txp.outputs, action:'sent', currency:sd.gcurrency, coin:sd.gcoin, rate:sd.grate}
            var data = {actionid:'100', atmid:sd.gatmid, timestamp:Date.now(), amount_coin:sd.gamount, amount:sd.bills, txpid:txpid, from:'sysaccount', to:address, fee:data.data.txp.fees, action:'sent', currency:sd.gcurrency, coin:sd.gcoin, rate:sd.gbrate}
            $.post(host + "users/log", JSON.stringify(data), function(data1){
                console.log('log something with result:' + data1.status);
            })
            buy_ok.show();
        }else{
            dialog.showMessageBox(remote.getCurrentWindow(), {type:'error', title:'network error!', message:data.msg});
            document.location.reload();
        }
        ipc.send('stop_loading');
    }).fail(function(){
        dialog.showMessageBox(remote.getCurrentWindow(), {type:'error', title:'network error!', message:'can not do pay in server try again!!'});
        document.location.reload();
    });
    //time out
    setTimeout(function(){
        document.location.replace("../index.html");
    }, 1200000);
})

function update_ui(){
    var grate = remote.getGlobal('shareData').gbrate;
    var bills = remote.getGlobal('shareData').bills;
    var currency = remote.getGlobal('shareData').gcurrency;
    var coin = remote.getGlobal('shareData').gcoin;
    var address = remote.getGlobal('shareData').save_address
    var amount = Number(bills) * 1.0 / Number(grate);
    $("#buy_rate").html(grate);
    $("#buy_notes").html('' + bills + ' ' + currency);
    $("#buy_coin").html('' + Number(amount).toFixed(5) + ' ' + coin);
    $("#buy_address").html(address);
    $("#buy_txpid").html(txpid);
}