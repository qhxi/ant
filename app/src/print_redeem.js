'use strict';
const remote = require('electron').remote
const QRCode = require('qrcode')
const ipc = require('electron').ipcRenderer

$(function () {
    var txid = remote.getGlobal('shareData').gtxid
    var canvas = document.getElementById('canvas');
    QRCode.toCanvas(canvas, txid, function (error) {
        if (error) console.error(error);
        console.log('success!');
    });
    console.log('print_redeem.js entered')
})

ipc.on('request-print', (event, args) => {
    setTimeout(() => {
        ipc.send("start-print", "")
    }, 100);
})