'use strict';
const remote = require('electron').remote
// var Web3EthIban = require('web3-eth-iban');

//global.coin_type global.amount global.rate
var address;

$(function () {
    const scan_ok = $('#scan_ok')
    scan_ok.on('click', (e) => {
        remote.getGlobal("shareData").save_address = address;
        document.location.href = "../uinew/btc_buy_ok.html";
    });

    var grate = remote.getGlobal('shareData').gbrate;
    $("#buy_rate").html(grate);
    scan_ok.hide()

    $('#scan_address').focus();

    //time out
    setTimeout(function(){
        document.location.replace("../uinew/btc_buy_error.html");
    }, 1200000);
})

$('onload', function(e){
    var code = "";
    var lastCode = null, nextCode = null;
    var lastTime = null, nextTime = null;

    this.onkeypress = function(e){
        if(e.which === 0x0D){
            if(lastCode !== null){
                code += String.fromCharCode(lastCode);
            }
            console.log('cr pressed and code is:' + code);
            setAddress(code);
            code = "";
            lastCode = null;
            lastTime = null;
        }else{
            if(lastCode === null && lastTime === null){
                console.log('set timeout in onkeypressed')
                setTimeout(() => {
                    console.log('scan wait 1 sec, result is:' + code + " last code is:" + lastCode)
                    if(code != "" && lastCode != null){
                        code += String.fromCharCode(lastCode);
                        setAddress(code);
                        code = "";
                        lastCode = null;
                        lastTime = null;
                    }
                }, 1000);
            }
            nextCode = e.which;
            nextTime = new Date().getTime();
            if(lastCode != null && lastTime != null && nextTime - lastTime <= 45) {
                code += String.fromCharCode(lastCode);
            } else if(lastCode != null && lastTime != null && nextTime - lastTime > 100){
                if(code !== ""){
                    code += String.fromCharCode(lastCode);
                }
                setAddress(code);
                code = "";
            }
            lastCode = nextCode;
            lastTime = nextTime;    
        }
    }
})

function setAddress(code){
    var codes = code.split(':');
    var gcoin = remote.getGlobal('shareData').gcoin
    console.log('set address codes is: ' + codes[0] + " : " + codes[1])
    if(codes[0] === 'bitcoin' && gcoin === 'btc'){
        console.log("codes0 is btc address")
        address = codes[1];
        $("#scan_address").html(address);
        $("#scan_ok").show();
    }else if(codes[0] === 'ethereum' && gcoin === 'eth'){
        console.log("codes0 is eth address")
        address = codes[1];
        $("#scan_address").html(address);
        $("#scan_ok").show();
    }else if(codes[0] === 'iban' && gcoin === 'eth'){
        var codes1 = codes[1].split('?');
        if(codes1[1] === 'token=ETH'){
            address = toEthAddress(codes1[0]);
            if(address === "" || address === undefined){
                return;
            }
            $("#scan_address").html(address);
            $("#scan_ok").show();
        }
    }else if(code.length > 20 && codes.length <= 1){
        if(gcoin === 'eth' && web3utils.isAddress(code)){
            address = code;
            $("#scan_address").html(address);
            $("#scan_ok").show();
        }else if(gcoin === 'btc'){
            address = code;
            $("#scan_address").html(address);
            $("#scan_ok").show();
        }
    }
}

function toEthAddress(addr) {
    const iban = require("web3-eth-iban");
    const web3utils = require("web3-utils");

    if (web3utils.isAddress(addr)) { //direct eth addr
        return addr.toString();
    } else if (iban.isValid(addr)) { // iban addr
        var ib = new iban(addr);
        if (ib.isDirect()) {
            return iban.toAddress(addr);
        } else {
            console.log("It isn't a direct bban addr");
            return ""
        }
    } else {
        return "";
    }
}

// function toEthAddress(code){
//     var iban = new Web3EthIban(code);
//     var address1 = iban.toAddress()
//     return address1;
// }