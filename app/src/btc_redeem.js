'use strict';
const remote = require('electron').remote
const QRCode = require('qrcode')
const ipc = require('electron').ipcRenderer

$(function (){
    // setInterval(query, 5000)
    $('#scan_txpid').focus();
    //time out
    setTimeout(function(){
        document.location.replace("../index.html");
    }, 1200000);
})

$('onload', function(e){
    var code = "";
    var lastTime = null, nextTime = null;
    var lastCode = null, nextCode = null;

    this.onkeypress = function(e){
        if(e.which === 0x0D){
            if(lastCode !== null){
                code += String.fromCharCode(lastCode);
            }
            console.log('cr pressed and code is:' + code);
            setTxp(code);
            code = "";
            lastCode = null;
            lastTime = null;
        }else{
            if(lastCode === null && lastTime === null){
                setTimeout(() => {
                    if(code != "" && lastCode != null){
                        console.log('time out 1 sec:code is:' + code)
                        code += String.fromCharCode(lastCode);
                        setTxp(code);
                        code = "";
                        lastCode = null;
                        lastTime = null;
                    }
                }, 1000);
            }
            nextCode = e.which;
            nextTime = new Date().getTime();
            if(lastCode != null && lastTime != null && nextTime - lastTime <= 30) {
                code += String.fromCharCode(lastCode);
            } else if(lastCode != null && lastTime != null && nextTime - lastTime > 100){
                if(code !== ""){
                    code += String.fromCharCode(lastCode);
                }
                setTxp(code);
                code = "";
            }
            lastCode = nextCode;
            lastTime = nextTime;    
        }
    } 
})

function setTxp(code){
    console.log('set txp id is: ' + code)
    if(code != ""){
        var txid = code;
        query1(txid);
    }
}

function query(){
    var txid = 'ae0dfc69721a8631ea28ccb4be2f9cadb9a8fd1fce0822597516e48784be9644'
    query1(txid);
}

function query1(txid){
    var gcoin = remote.getGlobal('shareData').gcoin
    var gatmid = remote.getGlobal('shareData').gatmid
    var host = remote.getGlobal('shareData').host
    $.getJSON(host + gcoin + "/querytx?atmid=" + gatmid + "&txpid=" + txid, function(data){
        if(Number(data.status) === 1 && data.data.txp.action === "received"){
            remote.getGlobal('shareData').gtxid = txid
            document.location.href = "../uinew/btc_sell_ok.html";
        }
    })
}

function on_click_scan(){
    console.log('on click scan')
    document.location.href = "../uinew/btc_sell_scan.html";
}

