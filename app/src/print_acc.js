'use strict';
const remote = require('electron').remote
const QRCode = require('qrcode')
const ipc = require('electron').ipcRenderer

console.log('print_acc.js entered');
$(function () {
    var pcoin = remote.getGlobal('shareData').print_coin
    var address = remote.getGlobal('shareData').print_address
    var private1 = remote.getGlobal('shareData').print_private

    var addr
    if (pcoin === 'btc'){
        addr  = "bitcoin:" + address;
    }else if(pcoin === 'eth'){
        addr = 'ethereum:' + address;
    }
    console.log('addr is:' + addr + ' pcoin is:' + pcoin + 'address is:' + address + private1)
    var canvas = document.getElementById('canvas');
    QRCode.toCanvas(canvas, addr, function (error) {
        if (error) console.error(error);
        console.log('success!');
    });
    var canvas1 = document.getElementById('canvas_private');
    QRCode.toCanvas(canvas1, private1, function (error) {
        if (error) console.error(error);
        console.log('success!');
    });
    $("#address").html(address)
    $("#privatekey").html(private1)
})

ipc.on('request-print', (event, args) => {
    setTimeout(() => {
        ipc.send("start-print", "")
    }, 100);
})