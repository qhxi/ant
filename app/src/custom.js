'use strict';
const remote = require('electron').remote
const dialog = require('electron').remote.dialog
const ipc = require('electron').ipcRenderer
const mac = require('macaddress')
const BrowserWindow = require('electron').remote.BrowserWindow

let alldata;
let first = true;
$(function() {
    const bitcoinbtn = $('#bitcoin'),
        etherbtn = $('.ether-btn'),
        litecoinbtn = $('.litecoin-btn'),
        dashbtn = $('.dash-btn');

    bitcoinbtn.on('click', (e) => {
        // global.gcoin = 'btc';
        remote.getGlobal('shareData').gcoin = 'btc'
        console.log("global.coin_type is:", global.coin_type)
        document.location.href="uinew/btc_trade.html";
        bitcoinbtn.text('bitcoin is clicked');
    });

    etherbtn.on('click', () => {
        remote.getGlobal('shareData').gcoin = 'eth'
        document.location.href="uinew/btc_trade.html";
        etherbtn.text('ethereum');
    });

    litecoinbtn.on('click', () =>{
        var k = {actionid:4, atmid:remote.getGlobal('shareData').gatmid, timestamp:Date.now(), scanst:0, printst:0, smartpayoutst:0, outcashst:0};
        $.post(remote.getGlobal('shareData').host + "users/log", JSON.stringify(k), function(data){
            console.log('log something with result:' + data.status);
        })                
        var winid = ipc.sendSync('get-devicewin-id', '');
        console.log('get device winid:' + winid)
        if(winid !== undefined && Number(winid) > 0){
            var dwin = BrowserWindow.fromId(Number(winid))
            console.log('get the device win:' + dwin);
            if(dwin){
                console.log('send to device win,to get status, from winid:' + remote.getCurrentWindow().id)
                // dwin.webContents.send('sdm2000-status', remote.getCurrentWindow().id);
		        // dwin.webContents.send('sdm2000-pay', 2, 0, remote.getCurrentWindow().id);
		        dwin.webContents.send('nv200-start', remote.getCurrentWindow().id);
	        }
        }
    });

    dashbtn.on('click', () =>{
        //test: get the item from database
        console.log('dashbtn pressed, try to pring redeem')
        // printredeem('mhpxBbAFJGh9o5qbubTSEdDX7qr1fBvgAw12345678test')
        var winid = ipc.sendSync('get-devicewin-id', '');
        console.log('get device winid:' + winid)
        if(winid !== undefined && Number(winid) > 0){
            var dwin = BrowserWindow.fromId(Number(winid))
            console.log('get the device win:' + dwin);
            if(dwin){
                console.log('send to device win,to get status, from winid:' + remote.getCurrentWindow().id)
                // dwin.webContents.send('sdm2000-status', remote.getCurrentWindow().id);
		        // dwin.webContents.send('sdm2000-pay', 0, 1, remote.getCurrentWindow().id);
		        dwin.webContents.send('nv200-end', remote.getCurrentWindow().id);
	        }
        }
    });

    //check the version of 
    if(first === true){
        first = false;
        ipc.send('start_loading');
        mac.one(function(err, addr){
            if(err){
                console.log('get mac address error:' + err + ' so quit!');
                remote.getCurrentWindow.close();
                return;                
            }
            var sn = addr.replace(/:/g,'');
            console.log('the final mac addr is:' + sn)
            var host = remote.getGlobal('shareData').host;
            var gatmid = remote.getGlobal('shareData').gatmid;
            $.getJSON(host + "users/checkserialnumber?atmid=" + gatmid + "&sn=" + sn, function(data){
                if(data.status == 1 && data.data.registered === true){
                    //get version ok
                    ipc.send('stop_loading');
                }else{
                    console.log('the sn is:' + sn + ' but not valid by server!')
                    // dialog.showErrorBox('Error not registed!', 'Atm sn is:' + sn)
                    dialog.showMessageBox(remote.getCurrentWindow(), {type:'error', title:'Error not registed!', message:'Atm sn is:' + sn});
                    ipc.send('quit-system');
                }
            });
        });
    }
});

ipc.on('nv200-polling-callback', (event, code, input) =>{
    var data = JSON.parse(input);
    if (code === 0xee){
        console.log('12345 nv200 polling status is:' + code + ' value is:' + data.total + 'currency is:' + data.currency);
    }else{
        console.log('12345 nv200 polling status is:' + code);
    }
})

ipc.on('sdm2000-status-res', (event, err, st) =>{
    console.log('sdm2000 get status return:' + st + ' error is:' + err);
})

ipc.on('sdm2000-pay-res', (event, err, st) =>{
    if(st !== null){
        console.log('sdm2000 pay return:' + st.toString('hex') + ' error is:' + err);
    }
})

function printaccount(type1, address, private1){
    remote.getGlobal("shareData").print_coin = type1
    remote.getGlobal("shareData").print_address = address
    remote.getGlobal("shareData").print_private = private1
    ipc.send('print-newaccount', '')
}

function printredeem(txid){
    remote.getGlobal("shareData").gtxid = txid
    ipc.send('print-redeem', '')
}
