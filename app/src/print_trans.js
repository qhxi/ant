'use strict';
const remote = require('electron').remote
const ipc = require('electron').ipcRenderer

$(function () {
    var txid = remote.getGlobal('shareData').gtxid
    var grate = remote.getGlobal('shareData').gbrate;
    var bills = remote.getGlobal('shareData').bills;
    var currency = remote.getGlobal('shareData').gcurrency;
    var coin = remote.getGlobal('shareData').gcoin;
    var address = remote.getGlobal('shareData').save_address
    var amount = Number(bills) * 1.0 / Number(grate);
    $("#buy_rate").html('buy price is:' + grate.toFixed(3));
    $("#buy_notes").html('cost notes:' + bills + ' ' + currency);
    $("#buy_coin").html('got coins ' + Number(amount).toFixed(8) + ' ' + coin);
    $("#buy_address").html('address is:' + address);
    $("#buy_txpid").html('transaction id:' + txid);
    console.log('print_trans.js entered')
})

ipc.on('request-print', (event, args) => {
    setTimeout(() => {
        ipc.send("start-print", "")
    }, 100);
})