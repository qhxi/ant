'use strict';
const remote = require('electron').remote
const ipc = require('electron').ipcRenderer

//global.coin_type global.amount global.rate
var start_time = Date.now();

$(function () {
    const choose_new = $('#choose_new'),
          choose_old = $('#choose_old')
    choose_new.on('click', (e) => {
        choose_new.disable = true;
        choose_old.disable = true;
        var gcoin = remote.getGlobal('shareData').gcoin
        var host = remote.getGlobal('shareData').host
        var atmid = remote.getGlobal('shareData').gatmid
        $.getJSON(host + gcoin + "/create").done(function(data){
            if(Number(data.status) === 1){
                var address = data.data.address;
                var priv = data.data.private;
                //fixed me: printer
                var k = {actionid:101, atmid:atmid, timestamp:Date.now(), address: address};
                $.post(host + "users/log", JSON.stringify(k), function(data){
                    console.log('log something with result:' + data.status);
                })
                remote.getGlobal("shareData").save_address = address;
                document.location.href = "../uinew/btc_buy_ok.html";
                printaccount(gcoin, address, priv)
            }else{
                dialog.showMessageBox(remote.getCurrentWindow(), {type:'error', title:'network error!', message:'can not create account in server, try again!!'});
                document.location.reload();
            }
        }).fail(function(){
            dialog.showMessageBox(remote.getCurrentWindow(), {type:'error', title:'network error!', message:'can not create account in server, try again!!'});
            document.location.reload();
        });
    });
    choose_old.on('click', (e) => {
        document.location.href = "../uinew/btc_buy_scan.html";
    });
    update_ui();
    //time out
    setTimeout(function(){
        document.location.replace("../uinew/btc_buy_error.html");
    }, 1200000);
})

function update_ui(){
    var grate = remote.getGlobal('shareData').gbrate;
    var bills = remote.getGlobal('shareData').bills;
    var currency = remote.getGlobal('shareData').gcurrency;
    var coin = remote.getGlobal('shareData').gcoin;
    var amount = Number(bills) * 1.0 / Number(grate);
    $("#buy_rate").html(grate);
    $("#buy_notes").html(':' + bills + ' ' + currency);
    // $("#buy_notes_type").html(currency);
    $("#buy_coin").html(':' + Number(amount).toFixed(5) + ' ' + coin);
}

function printaccount(type1, address, private1){
    remote.getGlobal("shareData").print_coin = type1
    remote.getGlobal("shareData").print_address = address
    remote.getGlobal("shareData").print_private = private1
    ipc.send('print-newaccount', '')
}