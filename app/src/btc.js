'use strict';
const remote = require('electron').remote
const dialog = require('electron').remote.dialog
const ipc = require('electron').ipcRenderer
const BrowserWindow = require('electron').remote.BrowserWindow

//global.coin_type global.amount global.rate
$(function () {
    const btc_buy = $('#btc_buy'),
        btc_sell = $('#btc_sell'),
        reminder = $('#reminder'),
        btc_sell1 = $('#sell_btn1'),
        btc_sell2 = $('#sell_btn2'),
        btc_sell3 = $('#sell_btn3'),
        btc_sell4 = $('#sell_btn4'),
        btc_sell5 = $('#sell_btn5'),
        btc_sell6 = $('#sell_btn6')

    var fees =  remote.getGlobal('shareData').gfees;
    btc_buy.on('click', (e) => {
        // ipc.send('load_url', 'file:'+__dirname +'/btc_wallet.html')
        btc_buy.disabled = true;
        var rate = remote.getGlobal('shareData').gbrate;
        remote.getGlobal('shareData').gbrate = rate * (1 + fees);
            document.location.href = "../uinew/btc_buy.html";
    });
    btc_buy.attr('disabled', true);

    btc_sell.on('click', () => {
        btc_sell.disabled = true;
        document.location.href = "../uinew/consent_agreement.html";
    });
    //btc_sell.disabled = true;
    btc_sell.attr('disabled', true);

    reminder.on('click', () => {
        document.location.href = "../uinew/btc_redeem.html";
    })
    reminder.attr('disabled', true);

    btc_sell1.on('click', () => {
        console.log('on click scan1')
        var bills = 10
        var rate = remote.getGlobal('shareData').gbrate * (1 - fees);
        remote.getGlobal('shareData').bills = bills
        remote.getGlobal('shareData').gamount = Number(bills * 1.0 / rate).toFixed(6)
        on_click_scan()
    });

    btc_sell2.on('click', () => {
        var bills = 20
        var rate = remote.getGlobal('shareData').gbrate *  (1 - fees);
        remote.getGlobal('shareData').bills = bills
        remote.getGlobal('shareData').gamount = Number(bills * 1.0 / rate).toFixed(6)
        on_click_scan()
    });

    btc_sell3.on('click', () => {
        var bills = 50
        var rate = remote.getGlobal('shareData').gbrate *  (1 - fees);
        remote.getGlobal('shareData').bills = bills
        remote.getGlobal('shareData').gamount = Number(bills * 1.0 / rate).toFixed(6)
        on_click_scan()
    });

    btc_sell4.on('click', () => {
        var bills = 100
        var rate = remote.getGlobal('shareData').gbrate *  (1 - fees);
        remote.getGlobal('shareData').bills = bills
        remote.getGlobal('shareData').gamount = Number(bills * 1.0 / rate).toFixed(6)
        on_click_scan()
    });

    btc_sell5.on('click', () => {
        var bills = 200
        var rate = remote.getGlobal('shareData').gbrate *  (1 - fees);
        remote.getGlobal('shareData').bills = bills
        remote.getGlobal('shareData').gamount = Number(bills * 1.0 / rate).toFixed(6)
        on_click_scan()
    });

    btc_sell6.on('click', () => {
        var bills = 500
        var rate = remote.getGlobal('shareData').gbrate *  (1 - fees);
        remote.getGlobal('shareData').bills = bills
        remote.getGlobal('shareData').gamount = Number(bills * 1.0 / rate).toFixed(6)
        on_click_scan()
    });

    var gcoin = remote.getGlobal('shareData').gcoin
    var min_amount = remote.getGlobal('shareData').minamount
    var gcurrency = remote.getGlobal('shareData').gcurrency
    console.log("global.coin_type is:", gcoin)
    if(gcoin != undefined){
        ipc.send('start_loading');
        $.getJSON(remote.getGlobal('shareData').host + "users/rate?coin=" + gcoin + "&currency=" + gcurrency)
        .done(function(data){
            console.log('get fiat rate return:' + data.status)
            if (data.status == 1){
                var rate = Number(data.data.price).toFixed(2);
                remote.getGlobal('shareData').gbrate = rate  //add some fee's can change
                console.log('the fiat rate is:' + rate)
                $("#buy_rate").html((rate * (1 + fees)).toFixed(2));
                $("#sell_rate").html((rate * (1 - fees)).toFixed(2));
                $("#sell_rate1").html(Number(10.0 / rate).toFixed(6) + gcoin)
                $("#sell_rate2").html(Number(20.0 / rate).toFixed(6) + gcoin)
                $("#sell_rate3").html(Number(50.0 / rate).toFixed(6) + gcoin)
                $("#sell_rate4").html(Number(100.0 / rate).toFixed(6) + gcoin)
                $("#sell_rate5").html(Number(200.0 / rate).toFixed(6) + gcoin)
                $("#sell_rate6").html(Number(500.0 / rate).toFixed(6) + gcoin)
            }
            $.getJSON(remote.getGlobal('shareData').host + gcoin + "/balance")
            .done(function(data){
                if (data.status == 1){
                    var amount = data.data.totalAmount;
                    if(gcoin === 'btc'){
                        amount = amount * 1.0 / 100000000;
                    }
                    if (amount > min_amount){
                        btc_buy.attr('disabled', false);
                    }
                }
                ipc.send('stop_loading')
            }).fail(function(jqxhr, textStatus, error){
                ipc.send('stop_loading')
                dialog.showMessageBox(remote.getCurrentWindow(),{type:'error', title:'network error!', message:'can not get data from server'});
                document.location.replace("../index.html");
            });
        }).fail(function(jqxhr, textStatus, error){
            ipc.send('stop_loading')
            dialog.showMessageBox(remote.getCurrentWindow(),{type:'error', title:'network error!', message:'can not get data from server'});
            document.location.replace("../index.html");
        });
    }

    var winid = ipc.sendSync('get-devicewin-id', '');
    if(winid !== undefined && winid > 0){
        var dwin = BrowserWindow.fromId(Number(winid))
        if(dwin){
            dwin.webContents.send('sdm2000-status', remote.getCurrentWindow().id);
        }
    }
})

ipc.on('sdm2000-status-res', function(event, err, arg){
    if(document.getElementById('btc_sell') == undefined){
        return;
    }
    if(arg){
        document.getElementById('btc_sell').disabled = false;
        document.getElementById('reminder').disabled = false;
    }else{
        document.getElementById('btc_sell').disabled = true;
    }
})

function ifshow() {
    // var id = document.getElementById("wallet_scan");
    // var flag = id.style.display == "";
    // if (flag) {
    //     id.style.display = "none";
    // } else {
    //     id.style.display = "";
    // }
    document.getElementById("btc_wallet_2").disabled = true;
    document.location.href="../uinew/btc_sell.html";
}

function on_click_scan(){
    console.log('on click scan11')
    //set the rate to sell rate
    var rate = remote.getGlobal('shareData').gbrate;
    var fees = remote.getGlobal('shareData').gfees;
    remote.getGlobal('shareData').gbrate = rate * (1 - fees);
    document.location.href = "../uinew/btc_sell_scan.html";
}

function create_account1() {
    document.getElementById("btc_wallet_1").disabled = true;
    document.getElementById("btc_wallet_2").disabled = true;
    $.getJSON(remote.getGlobal('shareData').host + gcoin + "/create", function (data) {
        if (data.status == 1){
            var address = data.data.address;
            var priv = data.data.private;
            // document.location.href="../ui/btc_sell.html";
        }
    });
}