'use strict';
const remote = require('electron').remote
const ipc = require('electron').ipcRenderer
const BrowserWindow = require('electron').remote.BrowserWindow

//global.coin_type global.amount global.rate
var bills = 0;
var totalnotesnum = 0;
$(function () {
    const buy_ok = $('#buy_ok'),
          buy_return = $('#buy_return')
    remote.getGlobal('shareData').bills = 0;
    buy_ok.on('click', (e) => {
        stop_smart_pay();
        document.location.href = "../uinew/btc_buy_choose.html";
    });
    buy_ok.hide()
    buy_return.on('click', (e) => {
        stop_smart_pay();
        document.location.href = "../index.html";
    });
    update_ui();
    query_smart_pay();

    //time out
    setTimeout(function(){
        document.location.replace("../uinew/btc_buy_error.html");
    }, 1200000);
})

function update_ui(){
    var grate = remote.getGlobal('shareData').gbrate;
    var bills = remote.getGlobal('shareData').bills;
    var currency = remote.getGlobal('shareData').gcurrency;
    var coin = remote.getGlobal('shareData').gcoin;
    var amount = Number(bills) * 1.0 / Number(grate);
    $("#buy_rate").html(grate);
    $("#buy_notes").html(':' + bills + ' ' + currency);
    // $("#buy_notes_type").html(currency);
    $("#buy_coin").html(':' + Number(amount).toFixed(5) + ' ' + coin);
    // $("#buy_coin_type").html(coin);
    if(Number(bills) > 0){
        remote.getGlobal('shareData').gamount = amount;
        $('#buy_ok').show();
        $('#buy_return').hide();
    }
}

function query_smart_pay(){
    var winid = ipc.sendSync('get-devicewin-id', '');
    console.log('get device winid:' + winid)
    if(winid !== undefined && Number(winid) > 0){
        var dwin = BrowserWindow.fromId(Number(winid))
        console.log('get the device win:' + dwin);
        if(dwin){
            console.log('send to device win,to get status, from winid:' + remote.getCurrentWindow().id)
            // dwin.webContents.send('sdm2000-status', remote.getCurrentWindow().id);
            // dwin.webContents.send('sdm2000-pay', 2, 0, remote.getCurrentWindow().id);
            dwin.webContents.send('nv200-start', remote.getCurrentWindow().id);
        }
    }
}

function stop_smart_pay(){
    var winid = ipc.sendSync('get-devicewin-id', '');
    console.log('get device winid:' + winid)
    if(winid !== undefined && Number(winid) > 0){
        var dwin = BrowserWindow.fromId(Number(winid))
        console.log('get the device win:' + dwin);
        if(dwin){
            console.log('send to device win,to get status, from winid:' + remote.getCurrentWindow().id)
            // dwin.webContents.send('sdm2000-status', remote.getCurrentWindow().id);
            // dwin.webContents.send('sdm2000-pay', 0, 1, remote.getCurrentWindow().id);
            dwin.webContents.send('nv200-end', remote.getCurrentWindow().id);
        }
    }
}

ipc.on('nv200-polling-callback', (event, code, input) =>{
    var data = JSON.parse(input);
    var sd = remote.getGlobal('shareData')
    var host = sd.host
    var bills = sd.bills;
    if (code === 0xee){
        console.log('12345 nv200 polling status is:' + code + ' value is:' + data.total + 'currency is:' + data.currency);
        //fixed me:should query the smart payout
        sd.bills = data.total;
        var data = {actionid:'3', atmid:sd.gatmid, timestamp:Date.now(), sub_action:'3', amount:data.total}
        $.post(host + "users/log", JSON.stringify(data), function(data1){
            console.log('log something with result:' + data1.status);
        })
        update_ui();
        if(Number(sd.bills) >= 500){
            stop_smart_pay();
        }
    }else{
        if(data.totalnotesnum !== totalnotesnum){
            console.log('total num is data.totalnum:' + data.totalnotesnum);
            totalnotesnum = data.totalnotesnum;
            var data = {actionid:'5', atmid:sd.gatmid, timestamp:Date.now(), totalnum:data.totalnotesnum};
            $.post(host + "users/log", JSON.stringify(data), function(data1){
                console.log('log something with result:' + data1.status);
            })    
        }
        console.log('12345 nv200 polling status is:' + code + 'data is:' + data);
    }
})