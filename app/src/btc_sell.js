'use strict';
const remote = require('electron').remote
const QRCode = require('qrcode')
const ipc = require('electron').ipcRenderer

//global.coin_type global.amount global.rate
var start_time = Date.now();
let timer
var printed = false;

$(function () {
    const btc_ok = $('#btc_ok')
    btc_ok.on('click', (e) => {
        document.location.href = "../uinew/btc_sell_ok.html";
    });
    btc_ok.hide()

    start_time = Date.now();
    var gamount = remote.getGlobal('shareData').gamount
    var gcoin = remote.getGlobal('shareData').gcoin
    var gatmid = remote.getGlobal('shareData').gatmid
    var rate = remote.getGlobal('shareData').gbrate
    var host = remote.getGlobal('shareData').host
    var bills = remote.getGlobal('shareData').bills;
    var currency = remote.getGlobal('shareData').gcurrency;

    $("#sell_amount").html(gamount + " " + gcoin);
    $("#sell_bills").html(bills + " " + currency);
    $("#sell_rate").html(rate);

    ipc.send('start_loading');
    $.getJSON(host + gcoin + "/sysaddress?atmid=" + gatmid).done(function(data){
        if(Number(data.status) === 1){
            var addr;
            if (gcoin === "btc"){
                addr = "bitcoin:" + data.data.address + "?amount=" + gamount;
            }else if(gcoin === 'eth'){
                addr = "ethereum:" + data.data.address + "?amount=" + gamount;
            }
            var canvas = document.getElementById('canvas');
            QRCode.toCanvas(canvas, addr, function (error) {
                if (error) console.error(error);
                console.log('success!');
            });
            $("#sell_addr").html(data.data.address)
    }else{
            dialog.showMessageBox(remote.getCurrentWindow(), {type:'error', title:'network error!', message:'can not get sys address from server'});
            document.location.replace("../index.html");
        }
        ipc.send('stop_loading');
    }).fail(function(){
        ipc.send('stop_loading')
        dialog.showMessageBox(remote.getCurrentWindow(), {type:'error', title:'network error!', message:'can not connect to server'});
        document.location.replace("../index.html");
    })
    timer = setInterval(query, 2000);
    //time out
    setTimeout(function(){
        document.location.replace("../index.html");
    }, 1200000);
})

function query(){
    var gcoin = remote.getGlobal('shareData').gcoin
    var gatmid = remote.getGlobal('shareData').gatmid
    var currency = remote.getGlobal('shareData').gcurrency
    var bills = remote.getGlobal('shareData').bills
    var host = remote.getGlobal('shareData').host
    var gamount = remote.getGlobal('shareData').gamount
    $.getJSON(host + gcoin + "/querytx?atmid=" + gatmid, function(data){
        if(Number(data.status) === 1 && data.data.txp.action === "received"){
            var cur_time = Number(data.data.txp.time);
            var amount = Number(data.data.txp.amount);
            console.log('cur time is:' + cur_time + 'start time is:' + (cur_time - (start_time / 1000.0)) + "the amount and gamount is:" + amount + 'gamount:' + gamount)
            if ((cur_time - (start_time / 1000.0)) < 10 * 60 && cur_time >  (start_time / 1000.0) && Math.abs(amount - gamount) < 0.01){
                clearInterval(timer);
                console.log('transaction is:' + data.data.txp.txid + 'cur time is:' + cur_time + 'start time is:' + start_time + 'amount is:' + amount)
                var txid = data.data.txp.txid;
                remote.getGlobal('shareData').gtxid = txid
                //fixed me:should put bellow
                var sd = remote.getGlobal('shareData')
                //txp time is less then
                var data = {txid:txid, atmid:gatmid, coin:gcoin, amount:amount, bills:bills, currency:currency}
                $.post(host + "users/settransaction", JSON.stringify(data), function(data1){
                    console.log('set the transaction to db ok:' + JSON.stringify(data));
                    printredeem(txid);
                    $('#btc_ok').show()
                })
                console.log('transaction is found')
            }
        }
    })
}

function printredeem(txid){
    if(!printed){
        printed = true;
        remote.getGlobal("shareData").gtxid = txid
        ipc.send('print-redeem', '')
    }
}

