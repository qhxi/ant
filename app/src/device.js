'use strict';
const remote = require('electron').remote
const ipc = require('electron').ipcRenderer
const SerialPort = require('serialport')
const BrowserWindow = require('electron').remote.BrowserWindow
const path = require('path');
const ffi = require('ffi')

let port, alldata, host, cmd_reset, cmd_diag, cmd_readst, cmd_payout, cmd_lastpay
let esdm2000 = 0, env200 = 0
let eprint, escan
let pay_cb, status_cb
let nv200dll, polling_back_win, nv200_total

var nv200_cb = ffi.Callback('void', ['int','string'], function(code, msg){
    console.log('enter the ffi.callback');
    if(polling_back_win === null){
        //nv200dll.endNv200();
        return;
    }
    var input = {};
    if(code === 0xee){
        var data = JSON.parse(msg);
        if(data && data.value){
            nv200_total += Number(data.value);
            input.currency = data.currency;
            input.total = nv200_total;
        }
        env200 = 0;
    }else{
        //encounter error!
        env200 = code;
    }
    var win = BrowserWindow.fromId(Number(polling_back_win));
    if(win){
        win.webContents.send('nv200-polling-callback', code, JSON.stringify(input));
    }
});

$(function() {
    //start the ffi nv200
    polling_back_win = null;
    var iopath = path.join(__dirname, '/../../nv200.dll');
    nv200dll = new ffi.Library(iopath, {
        'startNv200': ['int', ['int']],
        'endNv200': ['void', []],
        'empty': ['void', []],
        'getNotesNum': ['int', []],
        'setPolling': ['void', ['pointer']]
      });
    nv200dll.setPolling(nv200_cb);

    // //test: set the database
    host = remote.getGlobal('shareData').host
    cmd_readst = Buffer.from([0x02, 0x01, 0x31, 0x03])
    cmd_readst = addcheck(cmd_readst);
    cmd_diag = Buffer.from([0x02, 0x01, 0x32, 0x03])
    cmd_diag = addcheck(cmd_diag);
    cmd_payout = Buffer.from([0x02, 0x09, 0x3A, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03])
    cmd_payout = addcheck(cmd_payout);
    cmd_lastpay = Buffer.from([0x02, 0x01, 0x34, 0x03, 0x36])
    cmd_reset = Buffer.from([0x02, 0x01, 0x30, 0x03, 0x32])

    var sdm2000port = remote.getGlobal('shareData').port_sdm2000
    port = new SerialPort('COM' + sdm2000port, {
        baudRate: 9600,
        dataBits: 8,
        parity: 'none',
        stopBits: 1
    });

    port.on('error', (error) =>{
        console.log('sdm2000 port error:' + error);
        setTimeout(() => {
            port = new SerialPort('COM' + sdm2000port, {
                baudRate: 9600,
                dataBits: 8,
                parity: 'none',
                stopBits: 1
            });
        }, 1000);
    });

    port.on('data', (data) =>{
        console.log('on data:' + data.toString('hex'))
	    if(data.length === 1 && data.toString('hex') === '06'){
            console.log('on data single: 0x06')
            port.write(Buffer.from([0x05]));
            return;
        }
	    if(data.length === 1 && data.toString('hex') === '15'){
            console.log('on data single: 0x15')
            port.write(cmd_reset);
            return;
        }
        if(alldata === null || alldata === undefined){
            alldata = Buffer.from(data);
        }else{
            alldata = Buffer.concat([alldata, Buffer.from(data)]);
        }
        console.log('receive from com, alldata is:' + alldata.toString('hex'))
        while(alldata != null && alldata.length > 1 && alldata[0] !== 0x02){
            alldata = alldata.slice(1)
        }
        console.log('receive from com, after filter, alldata len is:' + alldata.length + ' alldata is:' + alldata.toString('hex'))
        if(alldata != null && alldata.length > 4){
            var len = alldata[1] + 4
            console.log('len is:' + len + 'alldata is:' + alldata.toString('hex'))
            if(Number(alldata.length) >= Number(len)){
                console.log('alldata 0:' + alldata[0] + 'alldata end:' + alldata[len -2]);
                if(alldata[0] == 0x02 && alldata[len - 2] == 0x03){
                    //like is the command
                    if(alldata[2] == 0x32){
                        port.write(Buffer.from([0x06]));
                        console.log('receive the cmd 0x32, return is:' + alldata.toString('hex'))
                        if(alldata[3] == 0x30){
                            //status is ok
                            esdm2000 = '0'
                            port.write(cmd_payout)
                        }else{
                            esdm2000 = alldata[3]
                            if(pay_cb)
                                pay_cb(esdm2000)
                            console.log("diagnostics status is error, code is" + alldata[3]);
                        }
                    }else if(alldata[2] == 0x3A){
                        port.write(Buffer.from([0x06]));
                        console.log('receive the cmd 0x3A, return is:' + alldata.toString('hex'))
                        if(alldata[3] == 0x30){
                            //success pay
                            port.write(cmd_lastpay);
                        }else{
                            esdm2000 = alldata[3]
                            if(pay_cb)
                                pay_cb(alldata[3])
                        }
                        console.log('not diagnotistic function!')
                    }else if(alldata[2] == 0x34){
                        console.log('receive the cmd 0x34, return is:' + alldata.toString('hex'))
                        port.write(Buffer.from([0x06]));
                        if(alldata[3] === 0x3A && alldata[4] === 0x30){
                            //success pay
                            if(pay_cb)
                                pay_cb(null, alldata.slice(0, len))
                        }else{
                            esdm2000 = alldata[3]
                            if(pay_cb)
                                pay_cb(alldata[3])
                        }
                        console.log('not diagnotistic function!')
                    }else if(alldata[2] == 0x31){
                        console.log('receive the cmd 0x31, return is:' + alldata.toString('hex'))
                        port.write(Buffer.from([0x06]));
                        if(alldata[3] === 0x30){
                            //success pay
                            var enough = ((alldata[5] & 0x2) === 0x2) && ((alldata[6] & 0x2) === 0x2);
                            if(status_cb)
                                status_cb(null, enough)
                        }else{
                            esdm2000 = alldata[3];
                            if(status_cb)
                                status_cb(esdm2000);
                        }
                        console.log('not diagnotistic function!')
                    }
                    alldata = alldata.slice(len - 1, alldata.length);
                }
            }
        }
    })
});

ipc.on('send-heart-beat', function(event, args){
    console.log('send the heart beat signal!')
    var shareData = remote.getGlobal('shareData');
    var k = {actionid:4, atmid:shareData.gatmid, timestamp:Date.now(), scanst:0, printst:0, smartpayoutst:env200, outcashst:esdm2000};
    $.post(host + "users/log", JSON.stringify(k), function(data){
        console.log('heat beat:' + data.status);
    })
});

ipc.on('sdm2000-pay', function(event, arg1, arg2, args){
    var winid = Number(args)
    console.log('sdm2000 start pay')
    if(arg1 !== undefined || arg2 !== undefined){
        pay(arg1, arg2, function(err, buf){
            console.log('sdm2000 pay called,' + err + ' result is:' + buf)
            var win = BrowserWindow.fromId(winid);
            if(win){
                console.log('pay win is:' + win)
                win.webContents.send('sdm2000-pay-res', err, buf);
            }
        });
    }
});

ipc.on('sdm2000-status', function(event, arg){
    var winid = Number(arg);
    console.log('get status start')
    if(winid !== undefined){
        readstatus(function(err, st){
            var win = BrowserWindow.fromId(winid);
            if(win){
                win.webContents.send('sdm2000-status-res', err, st);
            }
        })
    }
})

ipc.on('nv200-start', function(event, args){
    var port = remote.getGlobal('shareData').port_nv200
    console.log('nv200-start')
    if(polling_back_win !== null){
        console.log('the nv200 is already started!')
        return;
    }
    nv200_total = 0;
    polling_back_win = Number(args);
    console.log('polling_back_win in start is:' + polling_back_win)
    nv200dll.startNv200(Number(port));
    var num = nv200dll.getNotesNum();
    var input = {}
    input.totalnotesnum = num;
    var win = BrowserWindow.fromId(Number(polling_back_win));
    if(win){
        win.webContents.send('nv200-polling-callback', 0x00, JSON.stringify(input));
    }
});

ipc.on('nv200-end', function(event, args){
    console.log('nv200-end')
    polling_back_win = null;
    if(nv200_total > 0){
        nv200_total = 0;
        nv200dll.empty();
    }
    nv200dll.endNv200();
});

function pay(amount1, amount2, cb){
    console.log('cmd payout is:' + cmd_payout.toString('hex'))
    if(amount1 !== undefined){
        cmd_payout[3] = amount1
    }
    console.log('cmd payout1 is:' + cmd_payout.toString('hex'))
    if(amount2 !== undefined){
        cmd_payout[4] = amount2
    }
    console.log('cmd payout2 is:' + cmd_payout.toString('hex'))
    cmd_payout = cmd_payout.slice(0, 12);
    cmd_payout = addcheck(cmd_payout)
    console.log('cmd payout3 is:' + cmd_payout.toString('hex'))
    pay_cb = cb;
    console.log('pay args is:' + cmd_payout.toString('hex'));
    port.write(cmd_diag)
}

function readstatus(cb){
    console.log('start to read the status')
    status_cb = cb;
    status_cb(null, true);
    port.write(cmd_readst)
}

function addcheck(buf){
    var res = buf[1];
    for(var i = 2; i< buf.length; ++i){
        res = res ^ buf[i];
    }
    buf = Buffer.concat([buf, Buffer.from([res])])
    return buf;
}

function printaccount(type1, address, private1){
    remote.getGlobal("shareData").print_coin = type1
    remote.getGlobal("shareData").print_address = address
    remote.getGlobal("shareData").print_private = private1
    ipc.send('print-newaccount', '')
}

function printredeem(txid){
    remote.getGlobal("shareData").gtxid = txid
    ipc.send('print-redeem', '')
}