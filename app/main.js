const electron = require('electron')
const {app, BrowserWindow} = electron;
const path = require('path');
const url = require('url');
const fs = require('fs')
const ipc = electron.ipcMain;

let debug = /--debug/.test(process.argv[2]);
/*-------------the global's var-------------------
----global.coin_type: 'btc'
----global.currency: 'USD'
----global.amount: 0.012
----global.rate:  6323.32
//*/
let win, pwin, dwin, loadingWin;

function createWindow() {
    var count = 0, timer=null;
    pwin = null;
    dwin = null;
    console.log('config.debug is:' + debug)
    win = new BrowserWindow({width: 800, height: 600, frame:false});
    win.loadURL(path.join('file://', __dirname, '/index.html'));
    //win.webContents.openDevTools();
    win.maximize();
    if (debug) {
        win.webContents.openDevTools();
        //win.maximize();
        //require('devtron').install();
    }else{
    }

    debugger
    win.on('close', (event) => {
        console.log('close');
        if (process.platform != 'darwin') {
          event.preventDefault()
          win.hide()
        }
    })
    
    win.on('closed', () => {
        console.log('closed')
        win = null
        if (pwin){
            pwin.close();
            pwin = null;
        }
        if (dwin){
            dwin.close();
            dwin = null;
        }
        console.log('try to quit')
        app.quit()
    })

    //create the device window
    CreateDeviceWindows()

    timer = setInterval(function() {
            count++;
            if (count % 60 == 0) {
                dwin.webContents.send("send-heart-beat", "")
                //fixed me:update the device info
                // var k = {actionid:4, atmid:shareData.gatmid, timestamp:Date.now(), scanst:0, printst:0, smartpayoutst:0, outcashst:0};
                // $.post(host + "users/log", JSON.stringify(k), function(data){
                //     console.log('log something with result:' + data.status);
                // })                
            } else {
            }
    }, 500)
}

function LoadConfig(cb){
    var fpath = path.join( __dirname, '/../config.json');
    fs.exists(fpath, function (exists) {
        console.log('fpath is:' + fpath + 'exist?' + exists)
        if(exists){
            let config = JSON.parse(fs.readFileSync(fpath));
            if(config && config.host){
                shareData.host = config.host;
                shareData.gatmid = config.atmid;
                shareData.gfees = config.fees==undefined ? 0 : Number(config.fees);
                shareData.gcoin = config.coin == null ? "btc" : config.coin;
                shareData.gcurrency = config.currency == null ? "USD" : config.currency;
                if(config.sdm2000port != undefined){
                    shareData.port_sdm2000 = config.sdm2000port;
                }
                if(config.nv200port != undefined){
                    shareData.port_nv200 = config.nv200port;
                }
                if(config.outchannel1 != undefined){
                    shareData.out_channel1 = Number(config.outchannel1)
                }
                if(config.outchannel2 != undefined){
                    shareData.out_channel2 = Number(config.outchannel2)
                }
                debug = config.debug;
            }
            if(cb){
                cb();
            }
            console.log('the host is:' + shareData.host);
        }
    });
}

function CreateDeviceWindows(){
    if (dwin === null){
        console.log('create the device window')
        if(debug){
            dwin = new BrowserWindow({width:820, height:800, show: true});
            dwin.webContents.openDevTools();
        }else{
            dwin = new BrowserWindow({width:820, height:800, show: false});
        }
        dwin.webContents.on('did-finish-load', () => {
        })
    }
    console.log('load the device html')
    dwin.loadURL(path.join('file://', __dirname, '/ui/device.html'));
}

function startLoading(){
    if(!loadingWin){
        loadingWin = new BrowserWindow({parent:win, modal:true, center:true, width:96, height:96, frame:false});
        loadingWin.loadURL(path.join('file://', __dirname, '/ui/loading_win.html'));
        loadingWin.on('closed', () => {
            console.log('closed')
            loadingWin = null
        })
    }
    loadingWin.show();
}

function stopLoading(){
    if(loadingWin){
        loadingWin.hide();
        loadingWin.close();
        loadingWin = null;
    }
}

global.shareData = {
    gcoin:'eth',                            //coin type
    gcurrency : 'CNY',                      //note type
    gamount : 1,                            //current coin amount
    gbrate : 0,                             //current coint exchange rate
    gatmid : 'atm1',                        //the atmid
    gfees : 0,
    gtxid:'',                               //current transaction id
    bills:0,                                //current note amount
    save_address:'',                        //the address user want to save
    host:"",                                //the host of restful link
    print_address:'',
    print_private:'',
    print_coin:'',
    port_sdm2000:'1',
    port_nv200:'6',
    out_channel1:5,
    out_channel2:10,
    minamount:0.0001
}

app.on('ready', function(){
    //read the config.json
    LoadConfig(function(){
        createWindow();
    });
});
app.on('window-all-closed', () => {
    console.log('app try to quit')
    app.quit()
});

app.on('activate', () => {
    console.log('on activate!!! and win is' + win);
    if (win === null) {
        //read the config.json
        LoadConfig(function(){
            createWindow();
        });
        //createWindow()
    }else{
        win.show()
    }
});

ipc.on('get-devicewin-id', (event, arg) =>{
    console.log('main get dwin id:' + dwin.id);
    if(dwin === null){
        event.returnValue = '0';
    }
    event.returnValue = dwin.id.toString();
});

ipc.on("print-redeem", (event, arg) => {
    if (pwin === null){
        pwin = new BrowserWindow({width:220, height:800, show: false});
        pwin.webContents.on('did-finish-load', () => {
            pwin.webContents.send('request-print', '');
        })
    }
    pwin.loadURL(path.join('file://', __dirname, '/ui/print_redeem.html'));
});

ipc.on("print-newaccount", (event, arg) => {
    if (pwin === null){
        pwin = new BrowserWindow({width:220, height:800, show: false});
        pwin.webContents.on('did-finish-load', () => {
            pwin.webContents.send('request-print', '');
        })
    }
    pwin.loadURL(path.join('file://', __dirname, '/ui/print_acc.html'));
});

ipc.on("print-trans", (event, arg) => {
    if (pwin === null){
        pwin = new BrowserWindow({width:220, height:800, show: false});
        pwin.webContents.on('did-finish-load', () => {
            pwin.webContents.send('request-print', '');
        })
    }
    pwin.loadURL(path.join('file://', __dirname, '/ui/print_trans.html'));
});

ipc.on("start-print", (event, arg) => {
    console.log('start to print!')
    pwin.webContents.print({silent: true, printBackground: false}, function(success){
        if(success){
            console.log('print success')
        }else{
            console.log('print failed')
        }
    });
})

ipc.on('load_url', (event, url) => {
    if(win){
        console.log(url)
        win.loadURL(url)
    }
})

ipc.on('quit-system', (event) => {
    console.log('try to quit system')
    if(win){
        console.log('close win')
        win.close();
    }
    if(dwin){
        console.log('close dwin')
        dwin.close()
        dwin = null;
    }
    if(pwin){
        console.log('close pwin')
        pwin.close()
        dwin = null;
    }
    if(loadingWin){
        loadingWin.close()
        loadingWin = null;
    }
    app.quit()
})

ipc.on('start_loading', (event) => {
    startLoading();
})

ipc.on('stop_loading', (event) => {
    stopLoading();
})
