var _ = require('lodash');

var clear = function(client, cb){
	client.getTxProposals({
		doNotVerify: false 
	}, function(err, x) {
		if(err){
		  cb({status:'2', msg:err});
		  return;
		}
		var len = x.length
		if (x.length > 0) {
			_.each(x, function(txp) {
				client.removeTxProposal(txp, function(err, txp){
					if (err){
					  console.log('reject error:', err);
					}
				});
			});
			cb(null, len);
		} else {
			console.log('* No Tx Proposals.');
			cb(null, 0);
		}
	});
}

module.exports = clear;
