
var sysaddress = function(client, cb){
  if(!cb){
    console.log('call sysaddress without call back!');
    return
  }
  client.getMainAddresses({
    doNotVerify: false 
  }, function(err, list) {
    if(err){
	cb({status:'2', msg:err});
    }
    if (list.length > 0) {
      	console.log('got address:' + list[0]);
	cb(null, list[0]);
    } else {
	cb({status:'2', msg:'no address found'});
    }
  });
}
module.exports = sysaddress

