var _ = require('lodash');
var fee = 1e2
var note = 'free to test in test net'

var payto = function(client, options, cb){
  if(!cb){
    console.log('call payto without callback error!');
    return
  }
  client.createTxProposal({
    outputs: [{
      toAddress: options.address,
      amount: parseInt(options.amount),
    }],
    message: options.note,
    feeLevel: 'economy',
  }, function(err, txp) {
    if(err){
      	console.log('create tx proposal error: ', err);
	cb({status : '2', msg : 'create transaction proposal error:' + err});
	return;
    }
    client.publishTxProposal({
      txp: txp
    }, function(err) {
      if(err){
      	console.log('publish tx proposal error: ', err);
	cb({status : '2', msg : 'publish transaction proposal error:' + err});
	return;
      }
      console.log('Tx created:ID %s [%s] RequiredSignatures:', txp.id, txp.status, txp.requiredSignatures);
      setTimeout(function(){
	client.getTxProposals({
   	  doNotVerify: false 
  	}, function(err, x) {
    	  if (x.length > 0) {
      	    _.each(x, function(txp) {
	      client.signTxProposal(txp, function(err, txp){
	        if (err){
		  console.log('sign error:', err);
		  cb({status : '2', msg : 'sign transaction error:' + err});
		  return;
		};
		client.broadcastTxProposal(txp, function(err, txp){
		  if (err){
		    console.log('broadcast tx error:', err);
		    cb({status : '2', msg : 'broadcast transaction error:' + err});
		    return;
		  };
		  console.log("the txp after broadcast is:", txp);
		  cb(null, txp);
		});
	      });
      	    });
    	  } else {
      	    console.log('* No Tx Proposals.');
    	  }
  	});
      }, 500);
    });
  });
}

module.exports = payto;
