var Client = require('bitcore-wallet-client');
var config = require('../config.json');
var fs = require('fs');

var create = function(req, res, callback){
	if (!callback){
		console.log("bitcoin create address should have callback!!!")
	}
	if (req.session.addr){
		console.log("already have address!!!");
		res.send({'status':'2', 'msg':'already have address!!!'});
	}else{
		var client = new Client({baseUrl: config.BWS_URL,verbose: false});
		if (!client){
			console.log("btc client create error!!!");
			res.send({'status':'2', 'msg':'btc client create error!!!'});
		}
		try{
			createWallet(client, function(address){
  				fs.writeFileSync('./data/' + address + '.dat', client.export());
				req.session.addr = address;
				req.session.client = client;
				console.log('client is:', client);
				callback(address, client.credentials.xPrivKey, client.getMnemonic());
			});
		}catch(err){
			res.send({'status':'2', 'msg':err});
		}
	}
};

var createWallet = function(client, cb){
    client.createWallet("The Wallet", "atm_app", 1, 1, {network: config.network}, function(err, secret) {
  	if (err) {
   		console.log('error: ',err);
		throw new Error(err);
  	};
  	// Handle err
  	console.log('Wallet Created. Share this secret with your copayers: ' + secret);
  	client.openWallet(function(err, ret) {
    		if (err) {
      			console.log('error: ', err);
			throw new Error(err);
    		};
    		console.log('\n\n** Wallet Info', ret); //TODO

    		if (ret == true || ret.wallet.status == 'complete') {
      			client.createAddress({}, function(err,addr){
        			if (err) {
          				console.log('error: ', err);
					throw new Error(err);
        			};
				if (!addr){
					console.log('error:create wallet address is null');
					throw new Error('address is null');
				}
        			console.log('\nReturn:', addr.address)
				if (cb){
					cb(addr.address);
				}
      			});
    		}
  	});
    });
}

module.exports = create;
