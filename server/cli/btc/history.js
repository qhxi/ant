var express = require('express');

var history = function(client, req, cb){
	if(!cb){
	  console.log('error history call without callback!!!');
	  return;
	}
	client.getTxHistory({includeExtendedInfo:true}, function(err, txs) {
 	    if (err) {
	      cb({status:'2', msg:'get transaction history error:'+err});
	      return;
    	    };
	    if (txs && cb) {
		 var lists = [];
		 var i = 0;
		 var txp;
		 for(var tx of txs){
		   if(tx.action && tx.action === 'received'){
		     lists[i] = tx;
		     i = i + 1;
		     if(!txp){
			txp = tx;
		     }
		   }
		   if(req.query.txpid && tx.txid == req.query.txpid){
		     txp = tx;
		   }
		 }
		 cb(null, txp);
	    }
	});
}

module.exports = history;

