var express = require('express');

var balance = function(req, res, cb){
	var client = req.session.client;
	client.getBalance(true, function(err, x) {
 		if (err) {
    			console.log('error: ', err);
			throw new Error('get balance error:' + err);
    		};
  		console.log('balance is:' + x.totalAmount + 'locked:( ' + x.lockedAmount + ')');
		if (cb) {
			cb(null, x);
		}
	});
}

module.exports = balance;

