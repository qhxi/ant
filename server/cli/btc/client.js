var Client = require('bitcore-wallet-client');
var config = require('../config.json');
var fs = require('fs');

var load = function(req, address, cb){ 
	var client = new Client({
  		baseUrl: config.BWS_URL,
  		verbose: false,
	});

	var str = fs.readFileSync('data/'+ address +'.dat')
	client.import(str)

	client.openWallet(function(err, ret) {
    		if (err) {
      			console.log('error: ', err);
			throw new Error('open wallet error:' + err);
    		};
    		if (ret == true || ret.wallet.status == 'complete') {
			req.session.addr = address;
			req.session.client = client;
			if (cb) {
				cb();
			}
    		}
	});
};

module.exports = load;
