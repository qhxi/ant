var _ = require('lodash');
var fee = 100e2
var note = 'free to test in test net'

var transfer = function(req, options, cb){
	var client = req.session.client;
 	client.createTxProposal({
    		outputs: [{
      			toAddress: options.address,
			amount: parseInt(options.amount),
		}],
		message: options.note,
    		feePerKb: fee,
  	}, function(err, txp) {
		if(err){
      			console.log('create tx proposal error: ', err);
			throw new Error('create tx proposal error:' + err);
		}
    		client.publishTxProposal({
      			txp: txp
    		}, function(err) {
			if(err){
      				console.log('publish tx proposal error: ', err);
				throw new Error('publish tx proposal error:' + err);
			}
      			console.log(' * Tx created: ID %s [%s] RequiredSignatures:',
        		txp.id, txp.status, txp.requiredSignatures);
			setTimeout(function(){
			    client.getTxProposals({
   			    	    doNotVerify: false 
  			    }, function(err, x) {
    				    if (x.length > 0) {
      					_.each(x, function(txp) {
						client.signTxProposal(txp, function(err, txp){
							if (err){
								console.log('sign error:', err);
								throw new Error('sign proposal error:' + err);
							};
							client.broadcastTxProposal(txp, function(err, txp){
								if (err){
									console.log('broadcast tx error:', err);
									throw new Error('broadcast tx error:' + err);
								};
								console.log("the txp after broadcast is:", txp);
								if (cb){
									cb(txp);
								}
							});
						});
      					});
    				} else {
      					console.log('* No Tx Proposals.');
    				}
  			    });
			}, 500);
    		});
  	});
}

module.exports = transfer;
