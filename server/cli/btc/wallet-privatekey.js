var Client = require('bitcore-wallet-client');


var fs = require('fs');
var BWS_INSTANCE_URL = 'https://bws.bitpay.com/bws/api'
var client = new Client({
  baseUrl: BWS_INSTANCE_URL,
  verbose: false,
});
var xpriv = 'tprv8ZgxMBicQKsPedLd24YpgSEKXLEd5Djnpb8Us9jsG68qSDqKSr5bZ4BqRSwj5CDrjgZCc9MeTFujqmUh4m1AV2mHrxw9nrU33QPCWsmoLUu';

client.importFromExtendedPrivateKey(xpriv, {}, function(err){
  if (err) {
    console.log('error: ', err);
    return
  };

  console.log('import success!');

  client.openWallet(function(err, ret) {
    if (err) {
      console.log('error: ', err);
      return
    };
    console.log('\n\nopen wallet:', ret); //TODO
//      client.createAddress({}, function(err,addr){
//        if (err) {
//          console.log('error: ', err);
//          return;
///        };
	client.getMainAddresses({}, function(err, list){
	  console.log("address count is:" + list.length);
	  console.log('address is:' + list[0].address);
	});
	client.getBalance(true, function(err, x){
	    if (err){
		console.log(err);
		return;
	    }
	    console.log('amount is:' + x.totalAmount + ' locked amount is:' + x.lockedAmount);
	});
  });
});

