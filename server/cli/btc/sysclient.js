var Client = require('bitcore-wallet-client');
var config = require('../config.json');
var database = require('../db/redis');
var create = require('./syscreate');
var fs = require('fs');

var creating=false;

var load = function(atmid, type, cb){
	if (!cb){
	  console.log('load without callback!');
	  return
	}
	if (type === 'atm' && !atmid){
	  cb({status:'2', msg : 'no field atmid!!!'});
	  return
	} 
	var xpriv;
	if(type === 'atm'){
	  database.get_record('atm_system_account', function(err, res){
	    if (err && err === 'empty'){
				if(creating){
					cb({status:'2', msg:"account is creating!"});
					return;
				}
				creating = true;
				create(function(err, client, address, priv){
		  		if(err){
						creating = false;
		    		cb({status:'2', msg:err});
		    		return;
		  		}
					var items = {}
					items[atmid] = {'address':address, 'xpriv':priv};
		  		database.set_record('atm_system_account', items, function(err, res){
						creating = false;
						if(err){
			  			cb({status:'2', msg:err});
			  			return;
						}
						cb(null, client);
		  		});
				});
			return;
	  	}else if(err){
	  		cb({status:'2', msg : 'no field atmid!!!'});
				return;
	  	}
	  	if (res[atmid] && res[atmid].xpriv){
	    	xpriv = res[atmid].xpriv;
	    	loadInternal(xpriv, cb);
	  	}else{
				//new account
				console.log('new an account!!!');
				if(creating){
					cb({status:'2', msg:"account is creating!"});
					return;
				}
				creating = true;
				create(function(err, client, address, priv){
		  		if(err){
						 cb({status:'2', msg:err});
						 creating = false;
		    		return;
		  		}
		  		res[atmid] = {'address':address, 'xpriv':priv};
		  		database.set_record('atm_system_account', res, function(err, res){
						creating = false;
						if(err){
			  			cb({status:'2', msg:err});
			  			return;
						}
						cb(null, client);
		  		});
				});
	  	}
	  });  
	}else{
	  xpriv = config.xPrivateKeys;
	  loadInternal(xpriv, cb);
	}
};

var loadInternal = function(xpriv, cb){
	var client = new Client({
  	  baseUrl: config.BWS_URL,
  	  verbose: false,
	});
	console.log('\n the private key is:' + xpriv);
	if(!xpriv){
	  cb({status:'2', msg:'sys account is invalid!'});
	  return
	}
	client.importFromExtendedPrivateKey(xpriv, {}, function(err){
	//client.importFromMnemonic(xpriv, {
	//    network: config.network
	//}, function(err){
	  if(err){
		console.log('get the sys wallet failed!!!, error:' + err);
		cb({status : '2', msg : 'get sys wallet failed:' + err});
		return
	  }
	  client.openWallet(function(err, ret) {
    		if (err) {
      			console.log('error: ', err);
			cb({status : '2', msg : 'err'});
			return
    		};
    		if (ret == true || ret.wallet.status == 'complete') {
			if (cb) {
			  cb(null, client);
			}
    		}
	  });
	});
}

module.exports = load;
