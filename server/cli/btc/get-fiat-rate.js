var express = require('express');
var config = require('../config.json');

var fiatrate = function(client, code, ts, cb){
	if (!cb){
	  console.log('get fiat rate error, call without callback!!!');
	  return
	}
	console.log('provider is:' + config.provider);
	var opts = {code:code, ts:ts, provider:config.provider};
	client.getFiatRate(opts, function(err, rates){
	  if(err){
	    cb({status:'2', msg:'get fiat rates err:' + err});
	    return;
	  }
	  cb(null, rates);
	});
	return
}

module.exports = fiatrate;

