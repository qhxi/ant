var btc_sys_load = require('./sysclient');
var btc_sys_balance = require('../btc/sysbalance');
var btc_sys_payto = require('../btc/syspayto');
var btc_sys_clear  = require('../btc/sysclear');
var config = require("../config.json");
var kue = require('kue');

var q = kue.createQueue({
  prefix: 'q',
  redis: {
    port: config.redis_port,
    host: config.redis_host,
    auth: config.redis_auth,
    db: 3, // if provided select a non-default redis db
    options: {
      // see https://github.com/mranney/node_redis#rediscreateclient
    }
  }
});

q.process('pay_queue', function(job, done){
    handler(job, done);
})

function handler(job, done){
    console.log('job is:' + job.data.atmid + job.data.type + job.data.address + job.data.amount);
    btc_sys_load(job.data.atmid, job.data.type, function(err, client){
        if(err){
          return done(err);
        }
        btc_sys_balance(client, function(err, amount){
            if(err){
                return done(err);
            }
	    var pamount = Number(job.data.amount) * 100000000;
	    if (pamount > amount.totalAmount){
		return done('amount locked or no amount');
	    }
            btc_sys_payto(client, {address:job.data.address, amount:Number(job.data.amount) * 100000000, note:"payed by atm"}, function(err, x){
                if(err){
                    //if encounter err, should clear the txs
                    btc_sys_clear(client, function(err1){
                        if(err1){
                            return done(err1);
                        }
                        return done(err);
                    });
                    return;
                }
                done(null, {id : job.id, txpid:x.txid});
            })    
        })
    })
}

module.exports = q;

