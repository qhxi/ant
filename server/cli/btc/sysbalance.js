var express = require('express');

var balance = function(client, cb){
	client.getBalance(true, function(err, x) {
 		if (err) {
    			console.log('error: ', err);
			if(cb){
			  cb({status:'2', msg:'get balance failed!'});
			  return
			}
    		};
  		console.log('balance is:' + x.totalAmount + 'locked:( ' + x.lockedAmount + ')');
		if (cb) {
			cb(null, x);
		}
	});
}

module.exports = balance;

