var _ = require('lodash');

var sign = function(req, cb){
	var client = req.session.client;
	client.getTxProposals({
		doNotVerify: false 
	}, function(err, x) {
		if (x.length > 0) {
			_.each(x, function(txp) {
				client.signTxProposal(txp, function(err, txp){
					if (err){
						console.log('sign error:', err);
						throw new Error('sign proposal error:' + err);
					};
					client.broadcastTxProposal(txp, function(err, txp){
						if (err){
							console.log('broadcast tx error:', err);
							throw new Error('broadcast tx error:' + err);
						};
						console.log("the txp after broadcast is:", txp);
						if (cb){
							cb(txp);
						}
					});
				});
			});
		} else {
			console.log('* No Tx Proposals.');
		}
	});
}

module.exports = sign;
