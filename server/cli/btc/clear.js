var _ = require('lodash');

var clear = function(req, cb){
	var client = req.session.client;
	client.getTxProposals({
		doNotVerify: false 
	}, function(err, x) {
		if (x.length > 0) {
			_.each(x, function(txp) {
				client.rejectTxProposal(txp, function(err, txp){
					if (err){
						console.log('reject error:', err);
						cb('reject proposal error:' + err);
					}
					if (cb){
						cb(null, txp);
				    	}
				});
			});
		} else {
			console.log('* No Tx Proposals.');
		}
	});
}

module.exports = clear;
