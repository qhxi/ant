var createError = require('http-errors');
var config = require('./config.json');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var amqp = require('amqplib/callback_api');
var btcRouter = require('./routes/btc');
var eth2Router = require('./routes/eth');
var ethRouter = require('./routes/etherscan');
var huobiRouter = require('./routes/huobi');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(session({
    secret: 'atm-app',
    cookie: {maxAge: 60 * 1000 * 30},
    // rolling: false,
    resave: true,
    saveUninitialized: true
}));
app.use(bodyParser({keepExtensions: true, uploadDir: '/tmp'}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.disable('etag');

app.use('/btc', btcRouter);
app.use('/users', usersRouter);
app.use('/eth2', eth2Router);
app.use('/eth', ethRouter);
app.use('/huobi', huobiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

/*
amqp.connect(config.MQ_URL, function(err, conn) {
  if(conn === undefined){
	console.log('open amqp failed, conn is:' + conn + 'err:' + err);
	setTimeout(function(){
		process.exit(0);
	},3000);
	return
  }
  conn.createChannel(function(err, ch) {
    var q = config.mq_queue;

    ch.assertQueue(q, {durable: false});
    ch.consume(q, function(msg) {
      console.log("test: [x] Received %s", msg.content.toString());
    }, {noAck: true});
  });
})
//*/
module.exports = app;
