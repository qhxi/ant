const config = require('../config.json');
const express = require('express');
const EthereumTx = require('ethereumjs-tx');
const etherscan = require('etherscan-api').init(config.ETHERSCAN_TOKEN);
const utils = require("web3-utils");
const hbp = require("../huobi/huobi");
const router = express.Router();
const ethutils = require('../eth/etherscan');
const pay_queue = require('../eth/payqueue');

var send_mq_msg = function (msg, cb) {
    amqp.connect(config.MQ_URL, function (err, conn) {
        if (err) {
            console.log('connect to rabbit mq failed:' + err);
        }
        console.log('connect rabbit mq success!');
        conn.createChannel(function (err, ch) {
            var q = config.mq_queue;

            ch.assertQueue(q, {durable: false});
            ch.sendToQueue(q, Buffer.from(msg));
            console.log(" [x] Sent %s", msg);
        });
        setTimeout(function () {
            conn.close();
            if (cb) cb();
        }, 500);
    });
};

router.get('/create', function (req, res) {
    try {
        // if (req.query.address) {
        console.log('info:try to create account');
        req.session.address = null;
        ethutils.create(req, function (account) {
            console.log('info: create account success!!! account is:', account);
            res.send({'status': '1', 'data': {"address": account.address, "private": account.privateKey}});
        });
    } catch (err) {
        console.log('login catch error:' + err);
        res.send({'status': '3', 'msg': 'catch error:' + err});
    }
});

router.get('/sysaddress', function (req, res) {
    ethutils.eth_sys_load(req, 'atm', function (err, account) {
        if (err) {
            res.send(err);
            return;
        } else {
            res.send({status: '1', data: {address: account.address}});
            return;
        }
    });
});

router.get('/balance', function (req, res) {
    var type = 'atm';
    if (req.query.atmid === null || req.query.atmid === undefined) {
        type = 'sys';
    }

    ethutils.eth_sys_load(req, type, function (err, account) {
        if (err) {
            res.send(err);
            return
        }
        ethutils.balance(account.address, function (err, balance) {
            if (err) {
                res.send(err);
                return
            }
            res.send({'status': '1', 'data': {totalAmount: balance}});
        });
    });
});

let idx = 0;
router.get('/paytoqueue', function (req, res) {
    var maxretry = 10;

    if (!req.query.address) {
        res.send({status: '2', msg: 'target address invalid!'});
        return;
    }
    if (!req.query.amount) {
        res.send({status: '2', msg: 'amount invalid!'});
        return;
    }
    var id = 'etham' + (++idx);
    var pays = pay_queue.create('eth_pay_queue', {
        id: id,
        atmid: '',
        type: 'sys',
        address: req.query.address,
        amount: req.query.amount,
        retry: 0,
        txhash: '',
        max_attempts: maxretry
        // }).attempts(5).backoff({delay: 120 * 1000, type: 'fixed'}).save();
    }).attempts(maxretry).backoff({delay: 100, type: 'fixed'}).save();
    pays.on('complete', function (result) {
        if (result && result.id) {
            console.log('payto done result id is: ' + result.id + ',  txpid is: ' + result.txpid);
            var item = {actionid: 105, id: result.id, txpid: result.txpid};
        }
    });
    res.send({status: '1', data: {txp: {txpid: id, fees: 0}}})
});

router.get('/payto', function (req, res) {
    if (!req.query.address) {
        res.send({status: '2', msg: 'target address invalid!'});
        return;
    }
    if (!req.query.amount) {
        res.send({status: '2', msg: 'amount invalid!'});
        return;
    }

    ethutils.eth_sys_load(req, 'sys', function (err, account) {
        if (err) {
            res.send(err);
            return
        }

        var txParams = {
            to: req.query.address,
            value: utils.toHex(utils.toWei(req.query.amount.toString(), 'ether')),
        };
        ethutils.do_transfer(account, txParams, function (err, tx) {
            if (err) {
                res.send(err);
                return
            }
            res.send({'status': '1', data: tx});
            return;

        });

    });
});

router.get('/clear', function (req, res) {
    ethutils.eth_sys_load(req, 'sys', async function (err, account) {
        if (err) {
            res.send(err);
            return
        }
        var txs = await ethutils.get_pendingTransaction(account.address);
        if (txs.len === '0') {
            res.send({status: '2', data: "no clear transaction"});
            return;
        }

        var list = JSON.parse(txs.data);
        console.log(list);

        var privateKey = ethutils.key_to_buf(account.privateKey);
        var gas = await ethutils.eth_gasPricePriority();

        for (var i = list.length - 1; i >= 0; i--) {
            var ret = ethutils.reverse_transaction(list[i], privateKey, gas);
            if (ret === -1) {
                res.send({status: '2', data: 'clear transaction  ' + list[i] + '  failed, please retry'});
                return;
            }
        }
        res.send({status: '1', data: txs});
    });
});

router.get('/getfiatrate', function (req, res) {
    try {
        hbp.market_rate(req.query.symbol).then(function (data) {
            if (data === null) {
                console.log('data is null');
                res.send({'status': '-1'});
                return
            }
            res.send({status: '1', data: {rates: data.tick.close}});
        });
    } catch (err) {
        console.log('accounts catch error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/querytx', function (req, res) {
    if (!req.query.atmid) {
        res.send({status: '2', msg: 'no atmid field'});
        return;
    }
    ethutils.eth_sys_load(req, 'atm', async function (err, account) {
        if (err) {
            res.send(err);
            return;
        } else {
            if (req.query.txpid) { //query txpid
                ethutils.eth_getTransactionByTxpid(req.query.txpid, account.address, function (err, data) {
                    if (err) {
                        console.log(err);
                        res.send(err);
                        return;
                    }
                    data.action = (account.address.toLowerCase() === data.to.toLowerCase()) ? "received" : "sent";
                    res.send(ethutils.fill_tx(data));
                    return;
                });
            } else { //query latest transaction
                var txs = await ethutils.get_pendingTransaction(account.address); //try to get pending txs from web
                if (txs.len > '0') {
                    var list = JSON.parse(txs.data);
                    ethutils.eth_getTransactionByTxpid(list[0], account.address, function (err, data) {
                        if (err) {
                            console.log(err);
                            res.send(err);
                            return
                        }
                        data.action = (account.address.toLowerCase() === data.to.toLowerCase()) ? "received" : "sent";
                        res.send(ethutils.fill_tx(data));
                        return;
                    });
                } else {
                    ethutils.eth_getLatestTransactionByAddress(account.address, function (err, data) {
                        if (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                        data.action = (account.address.toLowerCase() === data.to.toLowerCase()) ? "received" : "sent";
                        res.send(ethutils.fill_tx(data));
                        return;
                    });
                }
            }
        }
    });
});

module.exports = router;
