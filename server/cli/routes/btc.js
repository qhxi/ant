var express = require('express');
var amqp = require('amqplib/callback_api');
var config = require('../config.json');
var pay_queue = require('../btc/payqueue')
var btc_load = require('../btc/client');
var btc_sys_load = require('../btc/sysclient');
var btc_create = require('../btc/create');
var btc_balance = require('../btc/getbalance');
var btc_sys_balance = require('../btc/sysbalance');
var btc_transfer = require('../btc/transfer');
var btc_sys_payto = require('../btc/syspayto');
var btc_sys_clear  = require('../btc/sysclear');
var btc_get_fiat  = require('../btc/get-fiat-rate');
var btc_history = require('../btc/history');
var btc_sys_address = require('../btc/sysaddress');

var router = express.Router();

var send_mq_msg = function(msg, cb){
  amqp.connect(config.MQ_URL, function(err, conn) {
  	if(err){
			console.log('connect to rabbit mq failed:' + err);
  	}
    console.log('connect rabbit mq success!');
    conn.createChannel(function(err, ch) {
      var q = config.mq_queue;

      ch.assertQueue(q, {durable: false});
      ch.sendToQueue(q, Buffer.from(msg));
      console.log(" [x] Sent %s", msg);
    });
    setTimeout(function(){conn.close(); if(cb) cb();}, 500);
  });
};

/* GET home page. */
router.get('/', function(req, res, next) {
  send_mq_msg('test for the root!!!');
  res.render('index', { title: 'ant ATM API' });
});

router.get('/create', function(req, res){
    try{
	if(0){//req.query.address){
		if (req.session.addr != req.query.address){
			btc_load(req, req.query.address, function(){
				res.send({"status":"1", "address":req.query.address});
			});
		}else{
			res.send({'status':'4', 'msg':'already logined'});
		}
	}else{
		console.log('info:try to create wallet');
		req.session.addr = null;
		btc_create(req, res, function(addr, priv, seed){
			console.log('info: create wallet success!!! address is:', addr, priv);
			res.send({'status':'1', 'data':{'address':addr, 'private':priv, 'seed':seed, 'balance':'0'}});
		});
	}
    }catch(err){
	console.log('login catch error:'+err);
    	res.send({'status':'3', 'msg':'catch error:'+err});
    }
}).post('/create', function(req, res){
    try{
	if(0){//req.body.address){
		if (req.session.addr != req.body.address){
			btc_load(req, req.body.address, function(){
				res.send({"status":"1", "address":req.body.address});
			});
		}else{
			res.send({'status':'4', 'msg':'already logined'});
		}
	}else{
		console.log("wallet login req.body is:", req.body);
		req.session.addr = null;
		btc_create(req, res, function(addr, priv, seed){
			console.log('info: create wallet success!!! address is:', addr);
			res.send({'status':'1', 'data':{'address':addr, 'private':priv, 'seed':seed, 'balance':'0'}});
		});
	}
    }catch(err){
	console.log('login catch error:'+err);
    	res.send({'status':'3', 'msg':'catch error:'+err});
    }
});

router.get('/sysaddress', function(req, res){
  btc_sys_load(req.query.atmid, 'atm', function(err, client){
		if(err){
      res.send(err);
	  	return
		}
		btc_sys_address(client, function(err, addr){
	  	if(err){
      	    res.send(err);
	    return
	  	}
  	  res.send({status:'1', data:{address:addr.address}});
		});
  });
});

router.get('/balance', function(req, res){
  var type = 'atm';
  if(req.query.atmid === null || req.query.atmid === undefined){
    type = 'sys';
  }
  btc_sys_load(req.query.atmid, type,  function(err, client){
    if(err){
      res.send(err);
      return
    }
    console.log('system client is:' + client);
    btc_sys_balance(client, function(err, x){
      if(err){
      	res.send(err);
	return
      }
      res.send({'status': '1', 'data': {totalAmount: x.totalAmount, lockedAmount: x.lockedAmount}});
    });
  });
});

router.get('/transfer', function(req, res){
    try{
	if(req.session.addr){
		if (!req.query.address || !req.query.amount){
			res.send({'status':'2', 'msg':'address or amount must be set'});
			return;
		}
		var note = req.query.note === null ? req.query.note : 'no notes here!';
		btc_load(req, req.session.addr, function(){
			btc_transfer(req, {address:req.query.address, amount:req.query.amount, note:note}, function(x){
				res.send({'status':'1', 'data':{txp:x}});
				console.log('info:transfer success!!!');
			});
		});
	}else{
		res.send({'status':'2', 'msg':'login first!!!!'});
	}	
    }catch(err){
	console.log('transfer catch error:'+err);
    	res.send({'status':'3', 'msg':'catch error:'+err});
    }
});

router.get('/clear', function(req, res){
  btc_sys_load(req.query.atmid, 'sys', function(err, client){
    if(err){
      res.send(err);
      return
    }
    btc_sys_clear(client, function(err1, num){
	if(err1){
	  res.send(err1);
	  return
	}
	res.send({status:'1', data : {}});
    });
  });
});

let idx = 0;
router.get('/paytoqueue', function(req, res){
  if(!req.query.address){
    res.send({status:'2', msg:'target address invalid!'});
    return;
  }
  if(!req.query.amount){
    res.send({status:'2', msg:'amount invalid!'});
    return;
  }
	var id = 'am' + (++idx);
	var pays = pay_queue.create('pay_queue', {
		id : id,
		atmid : '',
		type : 'sys',
		address : req.query.address,
		amount : req.query.amount
	}).attempts(15).backoff( {delay: 120*1000, type:'fixed'} ).save()
	pays.on('complete', function(result){
		if(result && result.id){
			console.log('payto done result id is:' + result.id + 'txpid is:' + result.txpid);
			var item = {actionid: 105, timestamp:Date.now(), id : result.id, txpid:result.txpid}
			send_mq_msg(JSON.stringify(JSON.stringify(item)));
		}
	})
	pays.on('failed', function(errorMessage){
		var item = {actionid: 106, timestamp:Date.now(), id : id, msg:errorMessage, address:pays.address, amount:pays.amount, coin:'btc'}
		send_mq_msg(JSON.stringify(JSON.stringify(item)));
	})
	res.send({status : '1', data : {txp : {txpid : id, fees : 0.0}}})
})

router.get('/payto', function(req, res){
  if(!req.query.address){
    res.send({status:'2', msg:'target address invalid!'});
    return;
  }
  if(!req.query.amount){
    res.send({status:'2', msg:'amount invalid!'});
    return;
  }
  var note = req.query.note === null ? req.query.note : 'no notes here!';
  btc_sys_load(req.query.atmid, 'sys', function(err, client){
    if(err){
      res.send(err);
      return
    }
    btc_sys_payto(client, {address:req.query.address, amount:req.query.amount * 100000000, note:note}, function(err, x){
	if(err){
	  //if encounter err, should clear the txs
	  btc_sys_clear(client, function(err1, num){
	    if(err1){
	      res.send(err1);
	      return
	    }
	    res.send(err);
	    return
	  });
	  return;
	}
	x.to = x.outputs[0].address
	x.amount = x.amount * 1.0 / 100000000;
	x.fees = x.fee * 1.0 / 100000000;
	res.send({'status':'1', data:{txp:x}});
	console.log('info:transfer success!!!');
    })
  });
});

router.get('/shouldpaynote', function(req, res){
  //check if req.query.txid is in db
  if(req.query.txpid){
    //save to db
    res.send({status:'1', data:{}})
  }else{
    res.send({status:'2', msg:'invalid txpid!'})
  }
});

router.get('/getfiatrate', function(req, res){
  if(!req.query.code){
    res.send({status:'2', msg:'currency invalid!'});
    return;
  }
  if(!req.query.time){
    res.send({status:'2', msg:'timestamp invalid!'});
    return;
  }
  btc_sys_load(req.query.atmid, 'sys', function(err, client){
    if(err){
			res.send(err);
			return
    }else{
	btc_get_fiat(client, req.query.code, req.query.time, function(err, rates){
	  if(err){
	    res.send(err);
	  }else{
	    res.send({status:'1', data:{rates:rates}});
	  }
	});
    }
  });
});

router.get('/querytx', function(req, res){
  btc_sys_load(req.query.atmid, 'atm', function(err, client){
    if(err){
      res.send(err);
    }else{
      btc_history(client, req, function(err, txs){
	if(err){
      	  res.send(err);
	}else if(!txs){
	  res.send({status:'2', msg:'no transaction history'});
	}else{
		txs.to = txs.outputs[0].address;
		txs.amount = txs.amount * 1.0 / 100000000;
		txs.fees = txs.fees * 1.0 / 100000000;
	  res.send({status: '1', data: {txp:txs}});
	}
      });
    }
  });
});
module.exports = router;
