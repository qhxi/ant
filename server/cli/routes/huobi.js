var express = require('express');
var config = require('../config.json');
var hbp = require("../huobi/huobi");
// var amqp = require('amqplib/callback_api');
var _ = require('lodash');

var router = express.Router();

router.get('/market', function (req, res) {
    try {
        hbp.market_rate(req.query.symbol).then(function (data) {
            if (data === null) {
                console.log('data is null');
                res.send({'status': '-1'});
                return
            }
            res.send({'rate': data.tick.close});
        });
    } catch (err) {
        console.log('accounts catch error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/accounts', function (req, res) {
    try {
        hbp.get_account().then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send(data);
        });
    } catch (err) {
        console.log('accounts catch error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/balance', function (req, res) {
    try {
        hbp.get_balance().then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send(data);
        });
    } catch (err) {
        console.log('balance catch error:' + err);
        res.send({'status': '-1'});
    }
});

//example /huobi/orders?symbol=btcusdt&start=2008-01-01&end=2018-12-31&direct=prev&size=100
router.get('/orders', function (req, res) {
    try {
        hbp.get_open_orders(req.query.symbol, req.query.start, req.query.end,
            req.query.direct, req.query.size).then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send(data);
        });
    } catch (err) {
        console.log('list orders catch error:' + err);
        res.send({'status': '-1'});
    }
});

//example /huobi/orders/results?symbol=btcusdt
router.get('/orders/results', function (req, res) {
    try {
        hbp.get_order_matchresults(req.query.symbol).then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send(data);
        });
    } catch (err) {
        console.log('list orders catch error:' + err);
        res.send({'status': '-1'});
    }
});
//example /huobi/order/detail?id=67980
router.get('/order/detail', function (req, res) {
    try {
        hbp.get_order(req.query.id).then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send(data);
        });
    } catch (err) {
        console.log('order detail info catch error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/order/buy-market', function (req, res) {
    try {
        hbp.buy_market(req.query.symbol, req.query.amount).then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send({'orderid': data});
        });
    } catch (err) {
        console.log(' buy market error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/order/sell-market', function (req, res) {
    try {
        hbp.sell_market(req.query.symbol, req.query.amount).then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send({'orderid': data});
        });
    } catch (err) {
        console.log('sell market error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/order/cancel', function (req, res) {
    try {
        hbp.submitcancel(req.query.id).then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send(data);
        });
    } catch (err) {
        console.log('cancel order error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/withdrawal', function (req, res) {
    try {
        if (1) {
            hbp.withdrawal(req.query.address, req.query.coin, req.query.amount).then(function (data) {
                if (data === null) {
                    res.send({'msg': 'failed to withdrawal!'});
                    return
                }
		res.send({'withdraw-id': data});
            });
        }
    } catch (err) {
        console.log('withdrawal error:' + err);
        res.send({'status': '-1'});
    }
});


router.get('/deposit', function (req, res) {
    if (!req.query.amount) {
        res.send({status: '2', msg: 'amount invalid!'});
        return;
    }
    console.log(req.query.type);
    if (req.query.type !== 'atm' && req.query.type !== 'sys') {
        res.send({status: '2', msg: 'atm or sys is not set'});
        return;
    }
    var port = process.env.PORT || '9001';
    var note = req.query.note === null ? req.query.note : 'no notes here!';
    if (req.query.coin === 'btc') {
        var btc_sys_load = require('../btc/sysclient');
        var btc_sys_payto = require('../btc/syspayto');
        var btc_sys_clear = require('../btc/sysclear');

        btc_sys_load(req.query.atmid, req.query.type, function (err, client) {
            if (err) {
                res.send(err);
                return
            }
            btc_sys_payto(client, {
                address: config.huobi[port].btc,
                amount: req.query.amount,
                note: note
            }, function (err, x) {
                if (err) {
                    //if encounter err, should clear the txs
                    btc_sys_clear(client, function (err1, num) {
                        if (err1) {
                            res.send(err1);
                            return
                        }
                        res.send(err);
                        return
                    });
                    return;
                }
                res.send({'status': '1', data: {txp: x}});
                console.log('info:transfer success!!!');
            });
        });

    } else if (req.query.coin === 'eth') {
        var eth_sys_load = require('../eth/sysclient');
        var eth_sys_payto = require('../eth/syspayto');
        eth_sys_load(req.query.atmid, req.query.type, function (err, account) {
            if (err) {
                res.send(err);
                return
            }
            try {
                eth_sys_payto(account, {
                    address: config.huobi[port].eth,
                    amount: req.query.amount,
                    note: note
                }, function (err, x) {
                    if (err) { //if encounter err, should clear the txs
                        res.send(err);
                        return
                    }
                    res.send({'status': '1', data: {txp: x}});
                    console.log('info:transfer success!!!');
                });
            } catch (err) {
                res.send(err);
                return
            }
        });
    } else {
        res.send({status: '2', msg: 'coin type  invalid!'});
    }
});

router.get('/withdrawalcancel', function (req, res) {
    try {
        hbp.withdrawalcancel(req.query.id).then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send(data);
        });
    } catch (err) {
        console.log('withdrawalcancel error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/deposits', function (req, res) {
    try {
        hbp.get_dws(req.query.coin, "deposit", req.query.from, req.query.size).then(function (data) {
            if (data === null) {
                res.send({'status': '-1'});
                return
            }
            res.send(data);
        });
    } catch (err) {
        console.log('get deposits error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/withdrawals', function (req, res) {
    try {
        hbp.get_dws(req.query.coin, "withdraw", req.query.from, req.query.size)
            .then(function (data) {
                if (data === null) {
                    res.send({'status': '-1'});
                    return
                }
                res.send(data);
            });
    } catch (err) {
        console.log('get deposits error:' + err);
        res.send({'status': '-1'});
    }
});

module.exports = router;
