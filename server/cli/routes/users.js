var express = require('express');
var config = require('../config.json');
var amqp = require('amqplib/callback_api');
var db = require('../db/redis');
var router = express.Router();
var http = require('../huobi/httpClient');
const request = require('request');

var send_mq_msg = function (msg, callback) {
    amqp.connect(config.MQ_URL, function (err, conn) {
        if (err) {
            console.log('connect to rabbit mq failed:' + err);
        }
        conn.createChannel(function (err, ch) {
            var q = config.mq_queue;

            ch.assertQueue(q, {durable: false});
            ch.sendToQueue(q, Buffer.from(msg));
            console.log(" [x] Sent %s", msg);
        });
        setTimeout(function () {
            conn.close();
            if (callback) {
                callback()
            }
        }, 500);
    });
};

router.post('/log', function (req, res) {
    if (req.body) {
        var item;
        for (var it in req.body) {
            item = it;
            break;
        }
        console.log('start to log, msg is:' + JSON.stringify(item));
        send_mq_msg(JSON.stringify(item), function () {
            res.send({status: '1', data: {}});
        });
    } else {
        res.send({status: '2', msg: 'invalide body msg'});
    }
});

router.get('/checkserialnumber', function (req, res) {
    if (req.query.sn && req.query.atmid) {
        db.get_record("redis_serialnumber", function (err, items) {
            if (err) {
                res.send({status: '2', msg: err});
                return;
            }
            if (items && items[req.query.atmid] === req.query.sn) {
                res.send({status: '1', data: {registered: true}});
            } else {
                res.send({status: '1', data: {registered: false}});
            }
        });
    } else {
        res.send({status: '2', msg: 'invalid params'});
    }
});

router.post('/setserialnumber', function (req, res) {
    if (req.body) {
        var item;
        for (var it in req.body) {
            item = it;
            break;
        }
        db.set_record("redis_serialnumber", JSON.parse(item), function (err, returns) {
            if (err) {
                res.send({status: '2', msg: err});
                return;
            }
            res.send({status: '1', data: returns});
        });
    } else {
        res.send({status: '2', msg: 'invalide body msg'});
    }
});

router.post('/settransaction', function (req, res) {
    if (req.body) {
        var item;
        for (var it in req.body) {
            item = it;
            break;
        }
        db.write_transaction(JSON.parse(item), function (err, items) {
            if (err) {
                res.send({status: '2', msg: err});
                return;
            }
            res.send({status: '1', data: {}});
        });
    } else {
        res.send({status: '2', msg: 'invalide body msg'});
    }
});

router.get('/gettransaction', function (req, res) {
    console.log('start to get transaction 1');
    if (req.query.txid) {
        console.log('start to get transaction 2');
        db.get_transaction(req.query.txid, function (err, item) {
            console.log('get transaction return:' + JSON.stringify(item));
            if (err) {
                res.send({status: '2', msg: err});
                return;
            }
            res.send({status: '1', data: item});
        });
    } else {
        res.send({status: '2', msg: 'invalide body msg'});
    }
});


function call_http_get(path) {
    return new Promise(function (resolve) {
        var url = path;
        console.log(url);
        var headers = {
            "Content-Type": "application/json"
        };
        http.get(url, {
            timeout: 1000,
            headers: headers
        }).then(function (data) {
            var json = JSON.parse(data);
            if (json.metadata.error === null) {
                console.log(json.data);
                resolve(json);
            } else {
                console.log('call error', json.metadata.error);
                resolve(null);
            }
        }).catch(function (ex) {
            console.log(method, path, 'exception', ex);
            resolve(null);
        });
    });
}


router.get('/rate', function (req, res) {
    var coin;

    if (!req.query.coin || !req.query.currency) {
        res.send({status: '-1', msg: 'missing coin or currency!'});
        return;
    }

    if (req.query.coin === "eth") {
        coin = "1027";
    } else {
        coin = "1";
    }
    var path = `https://api.coinmarketcap.com/v2/ticker/${coin}/?convert=${req.query.currency}`;
    call_http_get(path).then(function (ticker) {
        if (ticker === null) {
            res.send({'status': '-1'});
            return
        }

        var currency = req.query.currency.toUpperCase();
        res.send({status: '1', data: {coin:req.query.coin, currency: req.query.currency,
                price: ticker.data.quotes[''+currency+''].price, timestamp: ticker.metadata.timestamp }});
    });
});


/* GET users listing. */
//router.get('/', function(req, res, next) {
//  res.send('respond with a resource');
//});
router.get('/', function (req, res, next) {
    res.render('index', {title: 'ant ATM API'});
}).post('/', function (req, res) {
    res.render('index', {title: 'ant ATM API'});
});

module.exports = router;
