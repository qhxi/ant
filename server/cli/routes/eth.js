var express = require('express');
var router = express.Router();

var eth_load = require('../eth/client');
var eth_create = require('../eth/create');
// var eth_transfer = require('../eth/transfer');
var eth_sys_balance = require('../eth/sysbalance');
var eth_sys_load = require('../eth/sysclient');
var eth_sys_clear = require('../eth/sysclear');
var eth_history = require('../eth/history');
var eth_sys_payto = require('../eth/syspayto');
var hbp = require("../huobi/huobi");

var send_mq_msg = function (msg, cb) {
    amqp.connect(config.MQ_URL, function (err, conn) {
        if (err) {
            console.log('connect to rabbit mq failed:' + err);
        }
        console.log('connect rabbit mq success!');
        conn.createChannel(function (err, ch) {
            var q = config.mq_queue;

            ch.assertQueue(q, {durable: false});
            ch.sendToQueue(q, Buffer.from(msg));
            console.log(" [x] Sent %s", msg);
        });
        setTimeout(function () {
            conn.close();
            if (cb) cb();
        }, 500);
    });
};

router.get('/create', function (req, res) {
    try {
        // if (req.query.address) {
        if (0) {
            console.log('info:try to login account');
            if (req.session.address !== req.query.address) {
                eth_load(req, req.query.address, function () {
                    res.send({"status": "1", "address": req.query.address});
                });
            } else {
                res.send({'status': '4', 'msg': 'already logined'});
            }
        } else {
            console.log('info:try to create account');
            req.session.address = null;
            eth_create(req, res, function (account) {
                console.log('info: create account success!!! account is:', account);
                res.send({'status': '1', 'data': {"address": account.address, "private": account.privateKey}});
            });
        }
    } catch (err) {
        console.log('login catch error:' + err);
        res.send({'status': '3', 'msg': 'catch error:' + err});
    }
});

router.get('/sysaddress', function (req, res) {
    eth_sys_load(req.query.atmid, 'atm', function (err, account) {
        if (err) {
            res.send(err);
            return
        }
        res.send({status: '1', data: {address: account.address}});
    });
});

router.get('/balance', function (req, res) {
    eth_sys_load(req.query.atmid, 'sys', function (err, account) {
        if (err) {
            res.send(err);
            return
        }
        eth_sys_balance(account, function (err, x) {
            if (err) {
                res.send(err);
                return
            }
            res.send({'status': '1', 'data': {'address': account.address, 'balance': x}});
        });
    });
});

router.get('/payto', function (req, res) {
    if (!req.query.address) {
        res.send({status: '2', msg: 'target address invalid!'});
        return;
    }
    if (!req.query.amount) {
        res.send({status: '2', msg: 'amount invalid!'});
        return;
    }
    var note = req.query.note === null ? req.query.note : 'no notes here!';
    eth_sys_load(req.query.atmid, 'sys', function (err, account) {
        if (err) {
            res.send(err);
            return
        }

        try {
            eth_sys_payto(account, {
                address: req.query.address,
                amount: req.query.amount,
                note: note
            }, function (err, x) {
                if (err) { //if encounter err, should clear the txs
                    res.send(err);
                    return
                }
                res.send({'status': '1', data: {txp: x}});
                console.log('info:transfer success!!!');
            });
        } catch (err) {
            res.send(err);
            return
        }
    });
});

/*
router.get('/transfer', function (req, res) {
    try {
        // req.session.address = "0x01ead1b87E0761eE00290074a0cE2FE09A71c60a";
        // req.session.privateKey="0x9702075e09ed7f243e7283b6d7bba1d061733abe0b2267978ea301dcf1d6a2f7";
        console.log(req.session);
        if (req.session.address) {
            if (!req.query.address || !req.query.amount) {
                res.send({'status': '2', 'msg': 'address or amount must be set'});
                return;
            }
            var note = req.query.note === null ? req.query.note : 'no notes here!';
            eth_load(req, req.session.address, function () {
                eth_transfer(req, {
                    address: req.query.address,
                    amount: req.query.amount,
                    note: note
                }, function (err, x) {
                    if (err) {
                        // res.send({'status': '3', 'msg': 'transfer error:' + err});
                        res.send(err);
                        return
                    }
                    res.send({'status': '1', 'data': {txhash: x}});
                    console.log('info:transfer success!!!');
                });
            });
        } else {
            res.send({'status': '2', 'msg': 'login first!!!!'});
        }
    } catch (err) {
        console.log('transfer catch error:' + err);
        res.send({'status': '3', 'msg': 'catch error:' + err});
    }
});
*/

router.get('/clear', function (req, res) {
    eth_sys_load(req.query.atmid, 'sys', function (err, account) {
        if (err) {
            res.send(err);
            return
        }
        eth_sys_clear(account, function (err1, num) {
            if (err1) {
                res.send(err1);
                return
            }
            res.send({status: '1', data: {}});
        });
    });
});

router.get('/getfiatrate', function (req, res) {
    try {
        hbp.market_rate(req.query.symbol).then(function (data) {
            if (data === null) {
                console.log('data is null');
                res.send({'status': '-1'});
                return
            }
            res.send({'rate': data.tick.close});
        });
    } catch (err) {
        console.log('accounts catch error:' + err);
        res.send({'status': '-1'});
    }
});

router.get('/querytx', function (req, res) {
    if (!req.query.atmid) {
        res.send({status: '2', msg: 'no atmid field'});
        return;
    }
    eth_sys_load(req.query.atmid, 'atm', function (err, account) {
        if (err) {
            res.send(err);
        } else {
            eth_history(account, req, function (err, txs) {
                if (err) {
                    res.send(err);
                } else if (!txs) {
                    res.send({status: '2', msg: 'no transaction history'});
                } else {
                    res.send({status: '1', data: {txp: txs}});
                }
            });
        }
    });

});
module.exports = router;
