// var config = require('../config.json');
var fs = require('fs');
var web3 = require('./init');

var createAtmAccount = function (callback) {
    if (!callback) {
        console.log("ethereum create address should have callback!!!")
    }
    try {
        var account = {};
        var ret = web3.eth.accounts.create(web3.utils.randomHex(32));
        fs.writeFileSync('./eth_data/atm' + ret.address + '.dat', JSON.stringify(ret));
        req.session.address = ret.address;
        req.session.privateKey = ret.privateKey;

        account.address = ret.address;
        account.privateKey = ret.privateKey;

        callback(account);

    } catch (err) {
        res.send({'status': '02', 'msg': err});
    }
};
module.exports = createAtmAccount;
