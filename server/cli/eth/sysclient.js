var database = require('../db/redis');
var config = require('../config.json');
var fs = require('fs');
var create = require('./syscreate');
// var web3 = require('./init');

var load = function (atmid, type, cb) {
    if (!cb) {
        console.log('load without callback!');
        return
    }
    if (type === 'atm' && !atmid) {
        cb({status: '2', msg: 'no field atmid!!!'});
        return
    }
    var account={};
    if (type === 'atm') {
        database.get_record('atm_system_eth_account', function (err, res) {
            if (err && err === 'empty') {
                create(function (err, account) {
                    if (err) {
                        cb({status: '2', msg: err});
                        return;
                    }
                    var items = {atmid: {'address': account.address, 'xpriv': account.privateKey}};
                    database.set_record('atm_system_eth_account', items, function (err, res) {
                        if (err) {
                            cb({status: '2', msg: err});
                            return;
                        }
                        cb(null, account);
                    });
                });
                return;
            } else if (err) {
                cb({status: '2', msg: 'no field atmid!!!'});
                return;
            }
            if (res[atmid] && res[atmid].address) {
                account.address = res[atmid].address;
                account.privateKey = res[atmid].xpriv;
                cb(null, account);
            } else {
                //new account
                console.log('new an account!!!');
                create(function (err, account) {
                    if (err) {
                        cb({status: '2', msg: err});
                        return;
                    }
                    res[atmid] = {'address': account.address, 'xpriv': account.privateKey};
                    database.set_record('atm_system_eth_account', res, function (err, res) {
                        if (err) {
                            cb({status: '2', msg: err});
                            return;
                        }
                        cb(null, account);
                    });
                });
            }
        });
    } else {
        account.address = config.eth.address;
        account.privateKey = config.eth.privateKey;
        cb(null, account);
    }
};
module.exports = load;
