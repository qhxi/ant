const cheerio = require('cheerio');
const request = require('request');
const config = require('../config.json');
const express = require('express');
const EthereumTx = require('ethereumjs-tx');
const etherscan = require('etherscan-api').init(config.ETHERSCAN_TOKEN);
const utils = require("web3-utils");
const fs = require("fs");
const database = require('../db/redis');

function isNumber(val) {
    const regPos = /^\d+(\.\d+)?$/;
    const regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/;
    if (regPos.test(val) || regNeg.test(val)) {
        return true;
    }
    return false;
}

function parseGasStationPrice(price) {
    const beforeDecimal = price.toString().slice(0, -1);
    const afterDecimal = price.toString().slice(-1);
    return parseFloat(beforeDecimal + '.' + afterDecimal);
}


var ethutils = {
    key_to_buf: function (privateKey) {
        var str1 = privateKey.toString();
        var keystore = privateKey.toString().slice(2, str1.length);
        return new Buffer(keystore, 'hex');
    },

    fill_tx: function (tx) {
        var newtx = tx;
        newtx.txid = tx.hash;
        if (tx.blockNumber === null) newtx.confirmations = 0;
        else newtx.confirmations = 1;

        var fee = (tx.gas * tx.gasPrice).toString();
        newtx.fees = utils.fromWei(fee, 'ether');
        newtx.amount = utils.fromWei(tx.value, 'ether');
        console.log(newtx);
        return newtx;
    },

    create: function (req, callback) {
        const web3 = require('web3');
        const config = require('../config.json');

        var web3_instance = new web3(new web3.providers.HttpProvider(config.ETH_URL));
        try {
            var account = {};
            var ret = web3_instance.eth.accounts.create(utils.randomHex(32));

            ret.address = ret.address.toLowerCase();
            fs.writeFileSync('./eth_data/' + ret.address + '.dat', JSON.stringify(ret));
            req.session.address = ret.address;
            req.session.privateKey = ret.privateKey;

            account.address = ret.address;
            account.privateKey = ret.privateKey;
            if (callback) {
                callback(null, account);
            }
        } catch (err) {
            callback({'status': '02', 'msg': err});
        }
    },

    balance: function (address, cb) {
        etherscan.account.balance(address).then(function (res) {
            console.log(address + "  balance is  " + res.result);
            var balance = utils.fromWei(res.result, 'ether');
            if (cb) {
                cb(null, balance);
            }
        }, function (err) {
            console.error(err);
            cb({status: '2', msg: 'get balance failed!'});
        });
    },

    eth_sys_load: function (req, type, cb) {
        var atmid = req.query.atmid;
        if (!cb) {
            console.log('load without callback!');
            return
        }
        if (type === 'atm' && !atmid) {
            cb({status: '2', msg: 'no field atmid!!!'});
            return
        }
        var account = {};
        if (type === 'atm') {
            database.get_record('atm_system_eth_account', function (err, res) {
                if (err && err === 'empty') { // record
                    ethutils.create(req, function (err, account) {
                        if (err) {
                            console.error("err is empty");
                            cb({status: '2', msg: err});
                            return;
                        }
                        var items = {atmid: {'address': account.address, 'xpriv': account.privateKey}};
                        database.set_record('atm_system_eth_account', items, function (err, res) {
                            if (err) {
                                console.error("set record is empty");
                                cb({status: '2', msg: err});
                                return;
                            }
                            console.error("set account");
                            cb(null, account);
                        });
                    });
                    return;
                } else if (err) {
                    cb({status: '2', msg: err.toString()});
                    return;
                }

                if (res[atmid] && res[atmid].address) {
                    account.address = res[atmid].address;
                    account.privateKey = res[atmid].xpriv;
                    cb(null, account);
                } else {
                    //new account
                    ethutils.create(req, function (err, account) {
                        if (err) {
                            cb({status: '2', msg: err});
                            return;
                        }
                        res[atmid] = {'address': account.address, 'xpriv': account.privateKey};
                        database.set_record('atm_system_eth_account', res, function (err, res) {
                            if (err) {
                                cb({status: '2', msg: err});
                                return;
                            }
                            cb(null, account);
                        });
                    });
                }
            });
        } else {
            account.address = config.eth.address;
            account.privateKey = config.eth.privateKey;
            cb(null, account);
            return
        }
    },

    get_pendingTransaction: function (address) {
        return new Promise(function (resolve, reject) {
            var url = `https://etherscan.io/txsPending?a=` + address;
            request(url, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    var $ = cheerio.load(body);
                    var total = $("#spinwheel").parent().next().text().split(' ')[3];
                    console.log(total);
                    if (isNumber(total)) {
                        console.log("total pendingTransaction for addr " + address + "  is  " + total);
                    } else {
                        console.log("find total failed");
                        resolve({len: 0});
                        return;
                    }

                    var list = [];
                    var items = $(".address-tag a");
                    for (var i = 0; i < total; i++) {
                        list[i] = items[i].children[0].data;
                        console.log(list[i]);
                    }
                    resolve({len: total, data: JSON.stringify(list)});
                } else {
                    resolve({len: 0});
                }
            });
        });
    },

    reverse_transaction: function (oldtxp, privateKey, newgas) {
        return new Promise(function (resolve, reject) {
            etherscan.proxy.eth_getTransactionByHash(oldtxp).then(function (res) {
                if (res.result === null) {
                    console.log("no transaction found! ", res);
                    resolve(-1);
                    return;
                }
                var txinfo = res.result;
                if (txinfo.blockNumber === null) { //pending transaction
                    // console.log(txinfo);
                    var txParams = {
                        to: txinfo.from,
                        value: '0',
                        nonce: txinfo.nonce,
                        gasLimit: txinfo.gas,
                    };
                    txParams.gasPrice = newgas;
                    console.log(txParams);

                    const tx = new EthereumTx(txParams);
                    tx.sign(privateKey);
                    const serializedTx = tx.serialize().toString('hex');

                    etherscan.proxy.eth_sendRawTransaction('0x' + serializedTx).then(function (res) {
                        if (res === 'undefined' || res.result === 'undefined') {
                            console.log("try to get new transaction hash is failed");
                            resolve(-1);
                        } else {
                            console.log("clear transaction success! ", res);
                            resolve(0);
                        }
                        return;
                    }, function (err) {
                        console.error(err);
                        resolve(-1);
                        return;
                    });
                }
            }, function (err) {
                console.error(err);
                resolve(-1);
                return;
            });
        });
    },


    eth_getTransactionCount: function (address) {
        return new Promise(function (resolve, reject) {
            etherscan.proxy.eth_getTransactionCount(address)
                .then(function (res) {
                    resolve(res.result);
                    return;
                }, function (err) {
                    console.error("eth_getTransactionCount have error: ", err);
                    resolve(-1);
                    return;
                });
        });
    },


    eth_gasPricePriority: function () {
        var fetch = require('node-fetch');
        return new Promise(function (resolve, reject) {
            const url = 'https://ethgasstation.info/json/ethgasAPI.json';
            fetch(url).then(async (response, error) => {
                if (error) {
                    console.error("eth get gasPrice have error");
                    resolve(-1);
                }
                const gasData = await response.json();
                const gasPriceGweiStandard = parseGasStationPrice(gasData.safeLow);
                const gasPriceGweiPriority = parseGasStationPrice(gasData.fast);
                // console.log(gasPriceGweiStandard, gasPriceGweiPriority);
                resolve(utils.toHex(utils.toWei(gasPriceGweiPriority.toString(), 'Gwei')));
            });
        });
    },

    eth_gasPrice: function () {
        return new Promise(function (resolve, reject) {
            etherscan.proxy.eth_gasPrice().then(function (res) {
                resolve(res.result);
                return;
            }, function (err) {
                console.error("eth_gasPrice have error: ", err);
                resolve(-1);
                return;
            });
        });
    },

    eth_estimateGas: function (txParams) {
        return new Promise(function (resolve, reject) {
            const default_gas = 0xffffff;
            //to, value, gasPrice, gas
            etherscan.proxy.eth_estimateGas(txParams.to, txParams.value, txParams.gasPrice, default_gas)
                .then(function (res) {
                    console.log(res);
                    if (res.error) {
                        resolve(-1);
                    }
                    resolve(res.result);
                }, function (err) {
                    console.error("eth_estimateGas have err" + err);
                    resolve("-1");
                    return;
                });
        });
    },

    eth_getTransactionByHash: function (hash) {
        return new Promise(function (resolve, reject) {
                etherscan.proxy.eth_getTransactionByHash(hash).then(function (res) {
                    // console.log(res);
                    resolve(res.result);
                    return;
                }, function (err) {
                    console.error("eth_getTransactionByHash have error: ", err);
                    resolve(null);
                    return;
                });
            }
        )
    },

    eth_getLatestTransactionByAddress: function (address, cb) {
        return new Promise(function (resolve, reject) {
            const endblock = 9999999999;// set max block number
            const sort = 'desc'; // or asc
            const page = 1; // page number
            const offset = 1; // max records to return

            //txlist(address, startblock, endblock, page=1, offset=1, sort)
            etherscan.account.txlist(address, 0, endblock, page, offset, sort).then(function (res) {
                if (res.status === '1') {
                    cb(null, res.result[0]);
                } else {
                    cb({status: '2', msg: res.msg.message});
                }
                return;
            }, function (err) {
                cb({status: '2', msg: "Get transaction " + err.toString()});
                return;
            });
        });
    },

    eth_getTransactionByTxpid: function (txpid, address, cb) {
        return new Promise(function (resolve, reject) {
            etherscan.proxy.eth_getTransactionByHash(txpid).then(function (res) {
                console.log(res);
                if (res.result === null) {
                    cb({status: '2', msg: 'no transaction ' + txpid + ' for address ' + address});
                    return;
                } else if (res.result.to.toLowerCase() !== address.toLowerCase()) {
                    cb({status: '2', msg: 'tx ' + txpid + ' is not for require atm ' + address});
                    return;
                }
                cb(null, res.result);
                return;
            }, function (err) {
                console.error(err);
                cb({status: '2', msg: 'get transaction error'});
                return;
            });
        });
    },

    prepare_txParams: async function (account, txParams, cb) {
        let nonce = await ethutils.eth_getTransactionCount(account.address);
        if (nonce === -1) {
            cb({status: '2', msg: 'get nonce error!'});
            return;
        } else {
            txParams.nonce = nonce;
        }

        let gasPrice = await ethutils.eth_gasPrice();
        if (gasPrice === -1) {
            cb({status: '2', msg: 'get gasPrice error!'});
            return;
        } else {
            txParams.gasPrice = gasPrice;
        }

        if (1) {
            txParams.gasLimit = '0x5208'; // 21000 is the default value
        } else {
            let gas = await ethutils.eth_estimateGas(txParams);
            if (gas === -1) {
                cb({status: '2', msg: 'estimateGas error!'});
                return;
            } else {
                txParams.gasLimit = gas;
            }
        }
        cb(null, txParams);
        return;
    },

    payto_transfer: async function (account, txParams, cb) {
        let privateKey = ethutils.key_to_buf(account.privateKey);
        const tx = new EthereumTx(txParams);
        tx.sign(privateKey);
        const serializedTx = tx.serialize().toString('hex');
        // console.log(serializedTx);

        etherscan.proxy.eth_sendRawTransaction('0x' + serializedTx).then(async function (data) {
            if (data === undefined || data.result === undefined) {
                cb({status: '2', msg: 'payto failed, please retry!'});
                return;
            }
            for (let retry = 10; retry > 0; retry--) {
                let receipt = await ethutils.eth_getTransactionByHash(data.result);
                if (receipt !== null && receipt.blockNumber !== null) { // transaction is confirmed
                    let txp = receipt;
                    txp.action = (account.address.toLowerCase() === txp.to.toLowerCase()) ? "received" : "sent";
                    cb(null, ethutils.fill_tx(txp));
                    return;
                }
            }
            cb({'status': '2', msg: "payto success, but transaction  is not mined " + data.result});
            return
        }, function (err) {
            console.error(err);
            cb({status: '2', msg: err.toString()});
            return;
        });
    },

    do_transfer: async function (account, txParams, cb) {
        let nonce = await ethutils.eth_getTransactionCount(account.address);
        if (nonce === -1) {
            cb({status: '2', msg: 'get nonce error!'});
            return;
        } else {
            txParams.nonce = nonce;
        }
        let gasPrice = await ethutils.eth_gasPrice();
        if (gasPrice === -1) {
            cb({status: '2', msg: 'get gasPrice error!'});
            return;
        } else {
            txParams.gasPrice = gasPrice;
        }
        if (1) {
            txParams.gasLimit = 0x5208; // 21000 is the default value
        } else {
            let gas = await ethutils.eth_estimateGas(txParams);
            if (gas === -1) {
                cb({status: '2', msg: 'estimateGas error!'});
                return;
            } else {
                txParams.gasLimit = gas;
            }
        }
        console.log(txParams);

        // console.log(txParams.gasPrice*txParams.gasLimit);
        let privateKey = ethutils.key_to_buf(account.privateKey);
        const tx = new EthereumTx(txParams);
        tx.sign(privateKey);
        const serializedTx = tx.serialize().toString('hex');
        // console.log(serializedTx);

        etherscan.proxy.eth_sendRawTransaction('0x' + serializedTx).then(async function (data) {
            if (data === undefined || data.result === undefined) {
                cb({status: '2', msg: 'payto failed, please retry!'});
                return;
            }
            for (let retry = 5; retry > 0; retry--) {
                let receipt = await ethutils.eth_getTransactionByHash(data.result);
                if (receipt !== null) {
                    let txp = receipt;
                    txp.action = (account.address.toLowerCase() === txp.to.toLowerCase()) ? "received" : "sent";
                    cb(null, {txp: ethutils.fill_tx(txp)});
                    return;
                }
            }
            cb({'status': '2', msg: "payto success, but can not get txinfo for  " + data.result});
            return
        }, function (err) {
            console.error(err);
            cb({status: '2', msg: err.toString()});
            return;
        });
    },
};

module.exports = ethutils;