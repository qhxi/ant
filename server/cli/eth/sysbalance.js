var express = require('express');
var config = require('../config.json');
var web3 = require('./init');

var balance = function eth_balance(account, cb) {
    try {
        web3.eth.getBalance(account.address, function (err, ret) {
            if (err) {
                console.log('error: ', err);
                cb({status: '2', msg: 'get balance failed!'});
                return
            }
            if (cb) {
                cb(null, ret);
            }
        });
    } catch (err) {
        console.log('error: ', err);
    }
};
module.exports = balance;