var fs = require('fs');

var load = function (req, address, cb) {
    var account = {};
    try {
        var buff = fs.readFileSync('eth_data/' + address + '.dat');
        var str = JSON.parse(buff);
        req.session.address = str.address;
        req.session.privateKey = str.privateKey;
    } catch (err) {
        console.log('error: ', err);
        throw new Error('open account error:' + err);
    }
    if (cb) {
        cb();
    }
};

module.exports = load;

