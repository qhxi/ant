// var Web3 = require('web3');
var fs = require('fs');
var Tx = require('ethereumjs-tx');
const EthereumTx = require('ethereumjs-tx');
var config = require('../config.json');
var web3 = require('./init');

var transfer = function (req, options, cb) {
    var str1 = req.session.privateKey.toString();
    var keystr = req.session.privateKey.toString().slice(2, str1.length);
    var privateKey = new Buffer(keystr, 'hex');

    var txParams = {
        to: options.address,
        value: web3.utils.toWei(options.amount.toString(), 'ether') //options.amount.toString()
    };


    var eth_getTransactionCount = function (address) {
        return new Promise(function (resolve, reject) {
            web3.eth.getTransactionCount(address, function (error, count) {
                if (error) {
                    console.log("getTransactionCount error is " + error);
                    reject(error);
                } else {
                    console.log("current nonce is " + count);
                    resolve(count);
                }
            });
        });
    };

    var eth_getGasPrice = function () {
        return new Promise(function (resolve, reject) {
            web3.eth.getGasPrice(function (error, price) {
                if (error) {
                    console.log("getGasPrice error is " + error);
                    reject(error);
                } else {
                    console.log("current gasprice is " + price);
                    resolve(price);
                }
            });
        });
    };

    var eth_estimateGas = function (txParams) {
        return new Promise(function (resolve, reject) {
            web3.eth.estimateGas(txParams, function (error, gas) {
                if (error) {
                    console.log("estimateGas error is " + error);
                    reject(error);
                } else {
                    console.log("estimategas is " + gas);
                    resolve(gas);
                }
            });
        });
    };

    var eth_estimateGas = function (txParams) {
        return new Promise(function (resolve, reject) {
            web3.eth.estimateGas(txParams, function (error, gas) {
                if (error) {
                    console.log("estimateGas error is " + error);
                    reject(error);
                } else {
                    console.log("estimategas is " + gas);
                    resolve(gas);
                }
            });
        });
    };

    var get_txparams = async function (address, txParams) {
        txParams.nonce = await eth_getTransactionCount(address);
        txParams.gasPrice = await eth_getGasPrice();
        txParams.gas = await eth_estimateGas(txParams);
        console.log(txParams);

        const tx = new EthereumTx(txParams);
        tx.sign(privateKey);
        const serializedTx = tx.serialize();
        try {
            web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (error, receipt) {
                if (error) {
                    console.error("error is: ", error);
                    cb({status: '2', msg: "transfer error"});
                    return;
                }

                if (cb) {
                    console.log(receipt);
                    cb(null, receipt);
                    return
                }
            });
        } catch (err) {
            console.log(err);
            throw new Error(err);
        }
    };
    get_txparams(req.session.address, txParams);

};

module.exports = transfer;
