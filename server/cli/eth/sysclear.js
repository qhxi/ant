var config = require('../config.json');
var web3 = require('./init');
const EthereumTx = require('ethereumjs-tx');

var eth_getBlockNumber = function () {
    return new Promise(function (resolve, reject) {
        web3.eth.getBlockNumber(function (error, num) {
            if (error) {
                console.log("getTransactionCount error is " + error);
                reject(error);
            } else {
                // console.log("BlockNumber is " + num);
                resolve(num);
            }
        });
    });
};

var eth_getBlock = function (num) {
    return new Promise(function (resolve, reject) {
        web3.eth.getBlock(num, true, function (error, block) {
            if (error) {
                console.log("getBlock error is " + error);
                reject(error);
            } else {
                // console.log("getBlock: " + num);
                resolve(block);
            }
        });
    });
};

var clear = async function (address, cb) {
    var endBlockNumber = await eth_getBlockNumber();
    var startBlockNumber = (endBlockNumber - 9000) > 0 ? (endBlockNumber - 9000) : 0;

    var len = 0;
    for (var i = endBlockNumber; i >= startBlockNumber; i--) {
        var block = await eth_getBlock(i);
        if (block !== null && block.transactions !== null) {
            block.transactions.forEach(function (e) {
                if (address === e.from && e.transactionIndex === null) { // still pending
                    console.log(e);
                    // clearTransaction(e, account);
                    len++;

                }
            })
        }
    }
    cb(null, len);
};
// clear('0x01ead1b87E0761eE00290074a0cE2FE09A71c60a', console.log);

var clear2 = function (account, cb) {
    var startBlockNumber;
    var endBlockNumber = -1;
    web3.eth.getBlockNumber(function (error, ret) {
        if (error) {
            console.log("getBlockNumber error is", error);
            cb({status: '2', msg: 'err: getBlockNumber got error'});
            return;
        }
        if (ret === 0) {
            console.log("getBlockNumber return is", ret);
            cb({status: '2', msg: 'err: current eth node is not sync data!'});
            return;
        }
        endBlockNumber = ret;
    });
    startBlockNumber = (endBlockNumber - 1000) > 0 ? (endBlockNumber - 1000) : 0;
    console.log("Searching for transactions to/from account \"" + account.address + "\" within blocks " +
        startBlockNumber + " and " + endBlockNumber);

    // scan all blocks
    var len = 0;
    for (var i = startBlockNumber; i <= endBlockNumber; i++) {
        if (i % 1000 === 0) {
            console.log("Searching block " + i);
        }
        var block = web3.eth.getBlock(i, true);
        if (block !== null && block.transactions !== null) {
            block.transactions.forEach(function (e) {
                if (address === e.from && e.transactionIndex === null) {
                    console.log(e);
                    // clearTransaction(e, account);
                    len++;
                    cb(null, len);
                }
            })
        }
    }
};

function clearTransaction(txp, account) {
    const txParams = {
        nonce: txp.nonce,
        gasPrice: gasprice() * 1.1,
        // gasLimit: 24000,
        to: account.address,
        value: 0
    };
    const tx = new EthereumTx(txParams);
    tx.sign(account.privateKey);
    const serializedTx = tx.serialize();
    try {
        web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (error, receipt) {
            if (error) {
                console.error("sendSignedTransaction error is: ", error);
                cb({status: '2', msg: "sendSignedTransaction failed!"});
                return;
            }
            if (cb) {
                console.log("receipt is ", receipt);
                web3.eth.getTransaction(receipt, function (err, result) {
                    if (err) {
                        var errmsg = 'Get Receipt Error: ' + receipt;
                        console.error("error for receipt " + receipt);
                        cb({status: '2', msg: errmsg});
                        return;
                    }

                    var BN = web3.utils.BN;
                    var bngas = new BN(result.gas);
                    var bnprice = new BN(result.gasPrice);
                    var fee = web3.utils.fromWei(bngas.mul(bnprice).toString(), 'ether');
                    cb(null, {txid: receipt, 'fee': fee});
                });

            }
        });
    } catch (err) {
        console.log(err);
        cb({status: '2', msg: error});
    }
};

var account = {
    "address": "0x01ead1b87E0761eE00290074a0cE2FE09A71c60a",
    "privateKey": "0x9702075e09ed7f243e7283b6d7bba1d061733abe0b2267978ea301dcf1d6a2f7"
};

// clear(account, null);

module.exports = clear;
