// var Web3 = require('web3');
var config = require('../config.json');
var fs = require('fs');

var web3 = require('./init');
var create = function (req, res, callback) {
    try {
        var account = {};
        var ret = web3.eth.accounts.create(web3.utils.randomHex(32));
        fs.writeFileSync('./eth_data/' + ret.address + '.dat', JSON.stringify(ret));
        req.session.address = ret.address;
        req.session.privateKey = ret.privateKey;

        account.address = ret.address;
        account.privateKey=ret.privateKey;
        if (callback) {
            callback(account);
        }
    } catch (err) {
        res.send({'status': '02', 'msg': err});
    }
};
module.exports = create;