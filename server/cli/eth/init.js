var Web3 = require('web3');
var config = require('../config.json');

var web3_instance;

function web3_init() {
    try {
        if (typeof web3_instance !== 'undefined') {
            web3_instance = new Web3(web3_instance.currentProvider);

        } else {
            web3_instance = new Web3(new Web3.providers.HttpProvider(config.ETH_URL));
        }
        web3_instance.eth.net.getId().then('unhandledRejection', error => {
            console.error('ethereum init failed, please check eth node is started!!!');
//            process.exit(1) // To exit with a 'failure' code
        }).then(data => {
            console.log("ethereum network id is", data)
        });
    } catch (err) {
        console.error("web3 init failed: ", err);
        throw new Error('web3 init ' + err);
    }
}
// web3_init();
module.exports = web3_instance;

