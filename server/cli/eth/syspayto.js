var fs = require('fs');
var Tx = require('ethereumjs-tx');
const EthereumTx = require('ethereumjs-tx');
var config = require('../config.json');
var web3 = require('./init');

var payto = function (account, options, cb) {
    if (!cb) {
        console.log('call payto without callback error!');
        return
    }
    var str1 = account.privateKey.toString();
    var keystr = account.privateKey.toString().slice(2, str1.length);
    var privateKey = new Buffer(keystr, 'hex');

    var txParams = {
        to: options.address,
        value: web3.utils.toWei(options.amount.toString(), 'ether') //options.amount.toString()
    };

    if (1) {
        var eth_getTransactionCount = function (address) {
            return new Promise(function (resolve, reject) {
                web3.eth.getTransactionCount(address, function (error, count) {
                    if (error) {
                        console.log("getTransactionCount error is " + error);
                        cb({status: '2', msg: 'getTransactionCount error'});
                        //reject(error);
                    } else {
                        console.log("current nonce is " + count);
                        resolve(count);
                    }
                });
            });
        };

        var eth_getGasPrice = function () {
            return new Promise(function (resolve, reject) {
                web3.eth.getGasPrice(function (error, price) {
                    if (error) {
                        console.log("getGasPrice error is " + error);
                        //reject(error);
                        cb({status: '2', msg: 'getGasPrice error'});
                    } else {
                        console.log("current gasprice is " + price);
                        resolve(price);
                    }
                });
            });
        };

        var eth_estimateGas = function (txParams) {
            return new Promise(function (resolve, reject) {
                web3.eth.estimateGas(txParams, function (error, gas) {
                    if (error) {
                        console.log("estimateGas error is " + error);
                        cb({status: '2', msg: 'estimateGas error'});
                        //reject(error);
                    } else {
                        console.log("estimategas is " + gas);
                        resolve(gas);
                    }
                });
            });
        };

        let get_txparams = async function (address, txParams) {
            txParams.nonce = await eth_getTransactionCount(address);
            txParams.gasPrice = await eth_getGasPrice();
            txParams.gas = await eth_estimateGas(txParams);
            console.log(txParams);

            const tx = new EthereumTx(txParams);
            tx.sign(privateKey);
            const serializedTx = tx.serialize();
            try {
                web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (error, receipt) {
                    if (error) {
                        console.error("sendSignedTransaction error is: ", error);
                        cb({status: '2', msg: "sendSignedTransaction failed!"});
                        return;
                    }
                    if (cb) {
                        console.log("receipt is ", receipt);
                        web3.eth.getTransaction(receipt, function (err, result) {
                            if (err) {
                                var errmsg = 'Get Receipt Error: ' + receipt;
                                console.error("error for receipt " + receipt);
                                cb({status: '2', msg: errmsg});
                                return;
                            }

                            var BN = web3.utils.BN;
                            var bngas = new BN(result.gas);
                            var bnprice = new BN(result.gasPrice);
                            var fee = web3.utils.fromWei(bngas.mul(bnprice).toString(), 'ether');
                            cb(null, {txid: receipt, 'fee': fee});
                        });

                    }
                });
            } catch (err) {
                console.log(err);
                cb({status: '2', msg: error});
            }
        };
        get_txparams(account.address, txParams);
    }
};

module.exports = payto;
