var express = require('express');
var web3 = require('./init');

var history = function (account, req, cb) {
    if (!cb) {
        console.log('error history call without callback!!!');
        return;
    }
    if (req.query.txpid) {
        web3.eth.getTransaction(req.query.txpid, function (err, transaction) {
            if (err) {
                cb({status: '2', msg: 'get transaction history from txpid error:' + err});
                return
            }
            if (transaction.to.toLowerCase !== account.address.toLowerCase) {
                cb({status: '2', msg: 'err: txpid is not for this atm:' + req.query.txpid});
                return
            }
            if (transaction.blockHash === "") { //still pending
                cb({status: '2', msg: 'err: transaction state is pending:' + req.query.txpid});
                return
            }
            cb(null, transaction);
        });
    } else {
        getTransactionsByAccount(account.address, cb);
    }
};


function eth_getBlockNumber() {
    return new Promise(function (resolve, reject) {
        web3.eth.getBlockNumber(function (error, num) {
            if (error) {
                console.log("getTransactionCount error is " + error);
                reject(error);
            } else {
                // console.log("BlockNumber is " + num);
                resolve(num);
            }
        });
    });
}

function eth_getBlock(num) {
    return new Promise(function (resolve, reject) {
        web3.eth.getBlock(num, true, function (error, block) {
            if (error) {
                console.log("getBlock error is " + error);
                reject(error);
            } else {
                // console.log("getBlock: " + num);
                resolve(block);
            }
        });
    });
}

var getTransactionsByAccount = async function (address, cb) {
    var endBlockNumber = await eth_getBlockNumber();
    var startBlockNumber = 0;//(endBlockNumber - 9000) > 0 ? (endBlockNumber - 9000) : 0;


    var flag = 0;
    for (var i = endBlockNumber; i >= startBlockNumber; i--) {
        var block = await eth_getBlock(i);
        if (block !== null && block.transactions !== null) {
            block.transactions.every(function (e) {
                if (address === e.to && e.transactionIndex != null) {
                    if (flag === 0) {
                        console.log(e);
                        cb(null, e);
                        flag = 1;
                    }
                    return false; //break cycle
                }
                return true; //continue
            })
        }
    }
    if (flag === 0) { // not found
        cb({status: '2', msg: 'err: no transaction on address: ' + address});
    }

};

module.exports = history;

