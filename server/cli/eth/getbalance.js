// var Web3 = require('web3');
var express = require('express');
var config = require('../config.json');
var web3 = require('./init');

var balance = function eth_balance(req, res, cb) {
    try {
        web3.eth.getBalance(req.session.address, function (err, ret) {
            if (err) {
                console.log('error: ', err);
                cb({status: '2', msg: 'get balance failed!'});
                return
            }
            if (cb) {
                cb(null, ret);
            }
        });
    } catch (err) {
        console.log('error: ', err);
    }
};

module.exports = balance;

