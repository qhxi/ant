var config = require('../config.json');

var sysaddress = function (account, cb) {
    if (!cb) {
        console.log('call sysaddress without call back!');
        return
    }
    account.address = config.eth.address;
    account.privateKey = config.eth.privateKey;
    cb(null, account);
};
module.exports = sysaddress;

