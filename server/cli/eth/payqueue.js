const config = require("../config.json");
const kue = require('kue');
const ethutils = require('./etherscan');
const utils = require("web3-utils");
const jsrsasign = require('jsrsasign');
const EthereumTx = require('ethereumjs-tx');
const etherscan = require('etherscan-api').init(config.ETHERSCAN_TOKEN);

let q = kue.createQueue({
    prefix: 'ethq',
    redis: {
        port: config.redis_port,
        host: config.redis_host,
        // auth: config.redis_auth,
        db: 3, // if provided select a non-default redis db
        options: {
            // see https://github.com/mranney/node_redis#rediscreateclient
        }
    }
});


q.process('eth_pay_queue', function (job, done) {
    handler(job, done);
    job.data.retry++;
});

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

function handler(job, done) {
    console.log("job data is", job.data);
    const req = {};
    const query = {
        address: job.data.address,
        atmid: job.data.atmid,
        amount: job.data.amount
    };
    req.query = query;
    console.log('job is: atm ' + req.query.atmid + job.data.type + req.query.address + req.query.amount + " " + job.data.id);

    ethutils.eth_sys_load(req, job.data.type, function (err, account) {
        if (err) {
            return done(err);
        }

        ethutils.balance(account.address, function (err, balance) {
            if (err) {
                return done(err);
            }
            const txParams = {
                to: req.query.address,
                value: utils.toHex(utils.toWei(req.query.amount.toString(), 'ether')),
            };
            ethutils.prepare_txParams(account, txParams, function (err, data) {
                if (err) {
                    return done(err);
                }
                txParams.gasPrice = data.gasPrice;
                txParams.nonce = data.nonce;
                txParams.gasLimit = data.gasLimit.toString();

                balance = utils.toWei(balance, 'ether');
                var fee = (BigInt(txParams.gasLimit) * BigInt(txParams.gasPrice));
                if ((fee + BigInt(txParams.value)) > BigInt(balance)) {
                    console.log(fee + BigInt(txParams.value), BigInt(balance));
                    return done({status: '2', msg: 'fees + amount is over than balance'});
                }
                console.log(txParams);

                let privateKey = ethutils.key_to_buf(account.privateKey);
                const tx = new EthereumTx(txParams);
                tx.sign(privateKey);
                const serializedTx = tx.serialize().toString('hex');
                etherscan.proxy.eth_sendRawTransaction('0x' + serializedTx).then(async function (data) {
                    if (data === undefined || data.result === undefined) {
                        console.error("eth_sendRawTransaction no return");
                        return done({status: '2', msg: 'payto failed, please retry!'});
                    }
                    for (let retry = 60; retry > 0; retry--) {
                        let receipt = await ethutils.eth_getTransactionByHash(data.result);
                        if (receipt !== null && receipt.blockNumber !== null) { // transaction is confirmed
                            let txp = receipt;
                            txp.action = (account.address.toLowerCase() === txp.to.toLowerCase()) ? "received" : "sent";
                            console.log("tx " + data.result + " is mined");
                            return done(null, ethutils.fill_tx(txp));
                        }
                        console.log("tx " + data.result + " is still pending, retry is " + retry);
                        await sleep(8 * 1000);
                    }
                    job.data.txhash = data.result; // save transaction hash
                    return done({
                        'status': '2',
                        msg: "payto success, but transaction  is not mined " + data.result
                    });
                }, function (err) {
                    console.error(err);
                    return done({status: '2', msg: err.toString()});
                });
            });
        })
    })
}

module.exports = q;
