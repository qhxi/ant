var config = require('../config');
var CryptoJS = require('crypto-js');
var Promise = require('bluebird');
var moment = require('moment');
var HmacSHA256 = require('crypto-js/hmac-sha256');
var http = require('./httpClient');
var jsrsasign = require("jsrsasign");

const URL_HUOBI_PRO = 'api.huobipro.com';
// const URL_HUOBI_PRO = 'api.huobi.pro'; //备用地址

const DEFAULT_HEADERS = {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36"
};

var port = process.env.PORT || '9001';

function check_hbp_config() {
    if (config.huobi[port] === undefined) {
        console.log("huobi config for port " + port + " is not found!");
        throw new Error("huobi config for port " + port + " is not found!");
    }
}

function get_auth() {
    var sign = config.huobi[port].trade_password + 'hello, moto';
    var md5 = CryptoJS.MD5(sign).toString().toLowerCase();
    let ret = encodeURIComponent(JSON.stringify({
        assetPwd: md5
    }));
    return ret;
}

function sign_sha(method, baseurl, path, data) {
    var pars = [];
    for (let item in data) {
        pars.push(item + "=" + encodeURIComponent(data[item]));
    }
    var p = pars.sort().join("&");
    var meta = [method, baseurl, path, p].join('\n');
    // console.log(meta);
    var hash = HmacSHA256(meta, config.huobi[port].secretkey);
    var osig = CryptoJS.enc.Base64.stringify(hash);
    var Signature = encodeURIComponent(osig);
    // console.log(`Signature: ${Signature}`);
    p += `&Signature=${Signature}`;
    // console.log(p);
    var private_key = jsrsasign.KEYUTIL.getKey(config.huobi[port].privatekey);
    var sig = new jsrsasign.Signature({"alg": 'SHA256withECDSA', "prov": "cryptojs/jsrsa"});
    sig.init(private_key);
    var sigValueHex = sig.signString(osig);
    var wordArray = CryptoJS.enc.Hex.parse(sigValueHex);
    var ps = encodeURIComponent(CryptoJS.enc.Base64.stringify(wordArray));
    p += `&PrivateSignature=${ps}`;
    return p;
}

function get_body() {
    return {
        AccessKeyId: config.huobi[port].access_key,
        SignatureMethod: "HmacSHA256",
        SignatureVersion: 2,
        Timestamp: moment.utc().format('YYYY-MM-DDTHH:mm:ss')
    };
}

function call_api(method, path, payload, body) {
    return new Promise(function (resolve) {
        var account_id = config.huobi[port].account_id_pro;
        var url = `https://${URL_HUOBI_PRO}${path}?${payload}`;
        console.log(url);
        var headers = DEFAULT_HEADERS;
        headers.AuthData = get_auth();

        if (method === 'GET') {
            http.get(url, {
                timeout: 1000,
                headers: headers
            }).then(function (data) {
                let json = JSON.parse(data);
                if (json.status === 'ok') {
                    console.log(json.data);
                    resolve(json.data);
                } else {
                    console.log('call error', json);
                    resolve(null);
                }
            }).catch(function (ex) {
                console.log(method, path, 'exception', ex);
                resolve(null);
            });
        } else if (method === 'POST') {
            http.post(url, body, {
                timeout: 1000,
                headers: headers
            }).then(function (data) {
                let json = JSON.parse(data);
                if (json.status === 'ok') {
                    console.log(json.data);
                    resolve(json.data);
                } else {
                    console.log('call error', json);
                    resolve(null);
                }
            }).catch(function (ex) {
                console.log(method, path, 'exception', ex);
                resolve(null);
            });
        }
    });
}

function call_api_simple(method, path, body) {
    return new Promise(function (resolve) {
        var url = `https://${URL_HUOBI_PRO}${path}`;
        console.log(url);
        var headers = DEFAULT_HEADERS;

        if (method === 'GET') {
            http.get(url, {
                timeout: 1000,
                headers: headers
            }).then(function (data) {
                let json = JSON.parse(data);
                if (json.status === 'ok') {
                    console.log(json);
                    resolve(json);
                } else {
                    console.log('call error', json);
                    resolve(null);
                }
            }).catch(function (ex) {
                console.log(method, path, 'exception', ex);
                resolve(null);
            });
        } else if (method === 'POST') {
            http.post(url, body, {
                timeout: 1000,
                headers: headers
            }).then(function (data) {
                let json = JSON.parse(data);
                if (json.status === 'ok') {
                    console.log(json);
                    resolve(json);
                } else {
                    console.log('call error', json);
                    resolve(null);
                }
            }).catch(function (ex) {
                console.log(method, path, 'exception', ex);
                resolve(null);
            });
        }
    });
}

var huobi = {
    get_account: function () {
        var path = '/v1/account/accounts';
        var body = get_body();
        var payload = sign_sha('GET', URL_HUOBI_PRO, path, body);
        return call_api('GET', path, payload, body);
    },
    get_balance: function () {
        var account_id = config.huobi[port].account_id_pro;
        var path = `/v1/account/accounts/${account_id}/balance`;
        var body = get_body();
        var payload = sign_sha('GET', URL_HUOBI_PRO, path, body);
        return call_api('GET', path, payload, body);
    },
    get_open_orders: function (symbol, start, end, direct, size) {
        var path = '/v1/order/orders';
        var body = get_body();
        body.symbol = symbol;
        body.types = 'buy-market,sell-market,buy-limit,sell-limit,buy-ioc,sell-ioc';
        if (start) {
            body['start-date'] = start;
        }
        if (end) {
            body['end-date'] = end;
        }

        body.states = 'submitted,partial-filled,submitted,partial-filled,partial-canceled,filled,canceled';
        if (direct) {
            body.direct = direct;
        }
        if (size) {
            body.size = size;
        }
        var payload = sign_sha('GET', URL_HUOBI_PRO, path, body);
        return call_api('GET', path, payload, body);
    },
    get_order_matchresults: function (symbol) {
        var path = '/v1/order/matchresults';
        var body = get_body();
        body.symbol = symbol;
        body.types = 'buy-market,sell-market,buy-limit,sell-limit,buy-ioc,sell-ioc';
        body['start-date'] = "2008-01-01";
        var payload = sign_sha('GET', URL_HUOBI_PRO, path, body);
        return call_api('GET', path, payload, body);
    },
    get_order: function (order_id) {
        var path = `/v1/order/orders/${order_id}`;
        var body = get_body();
        var payload = sign_sha('GET', URL_HUOBI_PRO, path, body);
        return call_api('GET', path, payload, body);
    },
    buy_limit: function (symbol, amount, price) {
        var path = '/v1/order/orders/place';
        var body = get_body();
        body["account-id"] = config.huobi[port].account_id_pro;
        body.type = "buy-limit";
        body.amount = amount;
        body.symbol = symbol;
        body.price = price;
        var payload = sign_sha('POST', URL_HUOBI_PRO, path, body);
        return call_api('POST', path, payload, body);
    },
    buy_market: function (symbol, amount) {
        var path = '/v1/order/orders/place';
        var body = get_body();
        body["account-id"] = config.huobi[port].account_id_pro;
        body.type = "buy-market";
        body.amount = amount;
        body.symbol = symbol;
        var payload = sign_sha('POST', URL_HUOBI_PRO, path, body);

        return call_api('POST', path, payload, body);
    },
    sell_limit: function (symbol, amount, price) {
        var path = '/v1/order/orders/place';
        var body = get_body();
        body["account-id"] = config.huobi[port].account_id_pro;
        body.type = "sell-limit";
        body.amount = amount;
        body.symbol = symbol;
        body.price = price;
        var payload = sign_sha('POST', URL_HUOBI_PRO, path, body);

        return call_api('POST', path, payload, body);
    },
    sell_market: function (symbol, amount) {
        var path = '/v1/order/orders/place';
        var body = get_body();
        body["account-id"] = config.huobi[port].account_id_pro;
        body.type = "sell-market";
        body.amount = amount;
        body.symbol = symbol;
        var payload = sign_sha('POST', URL_HUOBI_PRO, path, body);

        return call_api('POST', path, payload, body);
    },
    submitcancel: function (order_id) {
        var path = `/v1/order/orders/${order_id}/submitcancel`;
        var body = get_body();
        var payload = sign_sha('POST', URL_HUOBI_PRO, path, body);
        return call_api('POST', path, payload, body);
    },
    withdrawal: function (address, coin, amount, payment_id) {
        var path = `/v1/dw/withdraw/api/create`;
        var body = get_body();
        body.address = address;
        body.amount = amount;
        body.currency = coin;
        if (coin.toLowerCase() === 'xrp') {
            if (payment_id) {
                body['addr-tag'] = payment_id;
            } else {
                console.log('huobi withdrawal', coin, 'no payment id provided, cancel withdrawal');
                return Promise.resolve(null);
            }
        }
        var payload = sign_sha('POST', URL_HUOBI_PRO, path, body);
        return call_api('POST', path, payload, body);
    },
    withdrawalcancel: function (withdrawid) {
        var path = `/v1/dw/withdraw-virtual/${withdrawid}/cancel`;
        var body = get_body();
        var payload = sign_sha('POST', URL_HUOBI_PRO, path, body);

        return call_api('POST', path, payload, body);
    },
    get_dws: function (coin, type, from, size) {
        var path = '/v1/query/deposit-withdraw';
        var body = get_body();
        body.type = type;
        body.from = from;
        body.size = size;
        body["currency"] = coin;
        var payload = sign_sha('GET', URL_HUOBI_PRO, path, body);
        return call_api('GET', path, payload, body);
    },
    market_rate: function (symbol) {
        var path = '/market/detail/merged';
        path += "?symbol=" + symbol;
        console.log(path);
        return call_api_simple('GET', path, null);
    }
};

function get_account_id() {
    if (config.huobi[port].account_id_pro === "") {
        huobi.get_account();
    }
}

get_account_id();
check_hbp_config();
module.exports = huobi;
