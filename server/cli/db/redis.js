var redis = require("redis");
var config = require("../config.json");
var database = {};

function connect(callback){
  var client = redis.createClient(config.redis_port, config.redis_host);
  client.on("error", function(err){
    console.log(err);
    if (callback){
    	callback(err, client);
    }
  });
  if (config.redis_auth){
    client.auth(config.redis_auth);
  }
  client.on("ready", function(res){
    if (callback){
      callback(null, client);
    }
  })
}

database.get_transaction = function(txid, callback){
  if (!callback){
    console.log("no callback in get_transaction");
    return
  }
 //check txid is valid?
  if (txid === null || typeof(txid) != 'string'){
    callback("invalid txid");
    return
  }
  connect(function(err, client){
    if (err){
	callback(err);	
	return
    }
    client.get("transaction_redis_key", function(err, res){
	if (err){
     	  client.end(true);
	  callback(err);
	  return
	}
	console.log('get transaction, result is:' + res);
	if (res != null && typeof(res) == 'string'){
	  res = JSON.parse(res);
	}
	if (res != null && res[txid] != null){
	  callback(null, res[txid])
	  delete res[txid];
  	  client.set("transaction_redis_key", JSON.stringify(res), function(err, res1){
    	    client.end(true);
	    if (err){
	      console.log("failed in get transaction, save transaction err");
	    }
	    console.log("test: the final record is:" + JSON.stringify(res1));
	  })
	}else{
	  //no record then return empty
	  callback(null, {});
	}
    });
  });
}

database.write_transaction = function(item, callback){
  if (!callback){
    console.log("no callback in get_transaction");
    return
  }
  //check item is valid?
  console.log('type of item' + typeof(item));
  if (item == null || typeof(item) != 'object'){
    callback("invalid item")
    return
  } 
  var txid = item['txid']
  if (	 txid == null ||
	 item['amount'] == null ||
	 item['bills'] == null ||
	 item['coin'] == null ||
	 item['currency'] == null ||
	 item['atmid'] == null){
    callback("invalid item datas");
    return;
  }
  connect(function(err, client){
   if (err){
	callback(err);	
	return
    }
    client.get("transaction_redis_key", function(err, res){
	if (err){
          client.end(true);
	  callback(err);
	  return
	}
	if (res === null){
	  res = {};
	}else if(typeof(res) == 'string'){
	  console.log('the res type is string :' + res);
	  res = JSON.parse(res);
	}else{
	  console.log('db data type invalid' + typeof(res));
	  res = {};
	}
	res[txid] = item;
	client.set("transaction_redis_key", JSON.stringify(res), function(err, res1){
          client.end(true);
	  if (err){
	    console.log("failed in write transaction, save transaction err");
	  }
	  console.log("test: the final record is:" + JSON.stringify(res1));
	  callback(null);
	})
    });
  });
}

database.get_record = function(redis_key, callback){
  if (!callback){
    console.log("no callback in get_transaction");
    return
  }
 //check txid is valid?
  if (redis_key === null || typeof(redis_key) != 'string'){
    callback("invalid key");
    return
  }
  connect(function(err, client){
    if (err){
	callback(err);	
	return
    }
    client.get(redis_key, function(err, res){
	if (err){
     	  client.end(true);
	  callback(err);
	  return
	}
	if (res != null && typeof(res) == 'string'){
	  res = JSON.parse(res);
	}
	if (res != null){
	  callback(null, res)
	}else{
	  //no record then return empty
	  callback('empty');
	}
     	client.end(true);
    });
  });
}

//obj is object type json 
database.set_record = function(redis_key, obj, callback){
  if (!callback){
    console.log("no callback in set record");
    return
  }
 //check txid is valid?
  if (obj === null || typeof(obj) != 'object'){
    callback("invalid data" + typeof(obj));
    return
  }
  if (redis_key === null || typeof(redis_key) != 'string'){
    callback("invalid key");
    return
  }
  connect(function(err, client){
    if (err){
	callback(err);	
	return
    }
    client.set(redis_key, JSON.stringify(obj), function(err, res){
	if (err){
     	  client.end(true);
	  callback(err);
	  return
	}
	callback(null, res)
     	client.end(true);
    });
  });
}




module.exports = database
