#!/bin/bash
function check_mongo() {
    for (( RETRY=30; $RETRY>0; $RETRY--)); do
        mongostat -h db -n 1
        if [[ $? -eq 0 ]];then
            echo "mongoDB start!"
            return 0;
        else
            sleep 1;
        fi
    done
    return -1
}

check_mongo
ret=$?

if [[ $ret -eq 0 ]];then
	cd /bitcore-wallet-service-1.15
	./stop.sh
	./start.sh
else
	exit -1
fi


