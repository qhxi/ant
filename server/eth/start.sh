#!/bin/bash

NETWORK=""
if [[ $TEST_NETWORK -eq 1 ]]
then
    echo "test network is set."
	NETWORK='--rinkeby'
fi

MINE=""
if [[ $ENABLE_MINE -eq 1 ]]
then
	echo "mine is set."
	MINE='--mine'
fi

/bin/geth --rpc --rpcaddr 0.0.0.0 --rpcapi="db,eth,net,web3,personal,web3" --syncmode fast --cache 1028 --datadir /data --rpccorsdomain 'http://localhost:8080' --verbosity 3 $NETWORK $MINE

